package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.MonitoringmsApp;
import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.repository.MainServerRepository;
import com.logicyel.monitoringms.repository.search.MainServerSearchRepository;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.mapper.MainServerMapper;
import com.logicyel.monitoringms.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;

import static com.logicyel.monitoringms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MainServerResource REST controller.
 *
 * @see MainServerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringmsApp.class)
public class MainServerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_URL = "AAAAAAAAAA";
    private static final String UPDATED_BASE_URL = "BBBBBBBBBB";

    @Autowired
    private MainServerRepository mainServerRepository;

    @Autowired
    private MainServerMapper mainServerMapper;

    @Autowired
    private MainServerService mainServerService;

    @Autowired
    private SubServerService subServerService;

    /**
     * This repository is mocked in the com.logicyel.monitoringms.repository.search TriggerQuartzStream package.
     *
     * @see com.logicyel.monitoringms.repository.search.TranscodingServerSearchRepositoryMockConfiguration
     */
    @Autowired
    private MainServerSearchRepository mockMainServerSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    @Autowired
    private StreamService streamService;

    private MockMvc restTranscodingServerMockMvc;

    private MainServer mainServer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MainServerResource mainServerResource = new MainServerResource(mainServerService, subServerService, streamService);
        this.restTranscodingServerMockMvc = MockMvcBuilders.standaloneSetup(mainServerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this TriggerQuartzStream.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they TriggerQuartzStream an entity which requires the current entity.
     */
    public static MainServer createEntity() {
        MainServer mainServer = new MainServer()
            .name(DEFAULT_NAME)
            .token(DEFAULT_TOKEN)
            .baseUrl(DEFAULT_BASE_URL);
        return mainServer;
    }

    @Before
    public void initTest() {
        mainServerRepository.deleteAll();
        mainServer = createEntity();
    }

    @Test
    public void createMainServer() throws Exception {
//        int databaseSizeBeforeCreate = mainServerRepository.findAll().size();
//
//        // Create the mainServer
//        MainServerDTO mainServerDTO = mainServerMapper.toDto(mainServer);
//        restTranscodingServerMockMvc.perform(post("/api/main-servers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mainServerDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the mainServer in the database
//        List<MainServer> mainServerList = mainServerRepository.findAll();
//        assertThat(mainServerList).hasSize(databaseSizeBeforeCreate + 1);
//        MainServer testMainServer = mainServerList.get(mainServerList.size() - 1);
//        assertThat(testMainServer.getName()).isEqualTo(DEFAULT_NAME);
//        assertThat(testMainServer.getToken()).isEqualTo(DEFAULT_TOKEN);
//        assertThat(testMainServer.getBaseUrl()).isEqualTo(DEFAULT_BASE_URL);
//
//        // Validate the mainServer in Elasticsearch
//        verify(mockMainServerSearchRepository, times(1)).save(testMainServer);
    }

    @Test
    public void createMainServerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mainServerRepository.findAll().size();

        // Create the mainServer with an existing ID
        mainServer.setId("existing_id");
        MainServerDTO mainServerDTO = mainServerMapper.toDto(mainServer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTranscodingServerMockMvc.perform(post("/api/main-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainServerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the mainServer in the database
        List<MainServer> mainServerList = mainServerRepository.findAll();
        assertThat(mainServerList).hasSize(databaseSizeBeforeCreate);

        // Validate the mainServer in Elasticsearch
        verify(mockMainServerSearchRepository, times(0)).save(mainServer);
    }

    @Test
    public void getAllMainServers() throws Exception {
        // Initialize the database
        mainServerRepository.save(mainServer);

        // Get all the mainServerList
        restTranscodingServerMockMvc.perform(get("/api/main-servers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mainServer.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].baseUrl").value(hasItem(DEFAULT_BASE_URL.toString())));
    }
    
    @Test
    public void getMainServer() throws Exception {
        // Initialize the database
        mainServerRepository.save(mainServer);

        // Get the mainServer
        restTranscodingServerMockMvc.perform(get("/api/main-servers/{id}", mainServer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mainServer.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN.toString()))
            .andExpect(jsonPath("$.baseUrl").value(DEFAULT_BASE_URL.toString()));
    }

    @Test
    public void getNonExistingMainServer() throws Exception {
        // Get the mainServer
        restTranscodingServerMockMvc.perform(get("/api/main-servers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateMainServer() throws Exception {
        // Initialize the database
        mainServerRepository.save(mainServer);

        int databaseSizeBeforeUpdate = mainServerRepository.findAll().size();

        // Update the mainServer
        MainServer updatedMainServer = mainServerRepository.findById(mainServer.getId()).get();
        updatedMainServer
            .name(UPDATED_NAME)
            .token(UPDATED_TOKEN)
            .baseUrl(UPDATED_BASE_URL);
        MainServerDTO mainServerDTO = mainServerMapper.toDto(updatedMainServer);

        restTranscodingServerMockMvc.perform(put("/api/main-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainServerDTO)))
            .andExpect(status().isOk());

        // Validate the mainServer in the database
        List<MainServer> mainServerList = mainServerRepository.findAll();
        assertThat(mainServerList).hasSize(databaseSizeBeforeUpdate);
        MainServer testMainServer = mainServerList.get(mainServerList.size() - 1);
        assertThat(testMainServer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMainServer.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testMainServer.getBaseUrl()).isEqualTo(UPDATED_BASE_URL);

        // Validate the mainServer in Elasticsearch
        verify(mockMainServerSearchRepository, times(1)).save(testMainServer);
    }

    @Test
    public void updateNonExistingMainServer() throws Exception {
        int databaseSizeBeforeUpdate = mainServerRepository.findAll().size();

        // Create the mainServer
        MainServerDTO mainServerDTO = mainServerMapper.toDto(mainServer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTranscodingServerMockMvc.perform(put("/api/main-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainServerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the mainServer in the database
        List<MainServer> mainServerList = mainServerRepository.findAll();
        assertThat(mainServerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the mainServer in Elasticsearch
        verify(mockMainServerSearchRepository, times(0)).save(mainServer);
    }

    @Test
    public void deleteMainServer() throws Exception {
        // Initialize the database
        mainServerRepository.save(mainServer);

        int databaseSizeBeforeDelete = mainServerRepository.findAll().size();

        // Delete the mainServer
        restTranscodingServerMockMvc.perform(delete("/api/main-servers/{id}", mainServer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MainServer> mainServerList = mainServerRepository.findAll();
        assertThat(mainServerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the mainServer in Elasticsearch
        verify(mockMainServerSearchRepository, times(1)).deleteById(mainServer.getId());
    }

    @Test
    public void searchMainServer() throws Exception {
        // Initialize the database
        mainServerRepository.save(mainServer);
        when(mockMainServerSearchRepository.search(queryStringQuery("id:" + mainServer.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(mainServer), PageRequest.of(0, 1), 1));
        // Search the mainServer
        restTranscodingServerMockMvc.perform(get("/api/_search/main-servers?query=id:" + mainServer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mainServer.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN)))
            .andExpect(jsonPath("$.[*].baseUrl").value(hasItem(DEFAULT_BASE_URL)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MainServer.class);
        MainServer mainServer1 = new MainServer();
        mainServer1.setId("id1");
        MainServer mainServer2 = new MainServer();
        mainServer2.setId(mainServer1.getId());
        assertThat(mainServer1).isEqualTo(mainServer2);
        mainServer2.setId("id2");
        assertThat(mainServer1).isNotEqualTo(mainServer2);
        mainServer1.setId(null);
        assertThat(mainServer1).isNotEqualTo(mainServer2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MainServerDTO.class);
        MainServerDTO mainServerDTO1 = new MainServerDTO();
        mainServerDTO1.setId("id1");
        MainServerDTO mainServerDTO2 = new MainServerDTO();
        assertThat(mainServerDTO1).isNotEqualTo(mainServerDTO2);
        mainServerDTO2.setId(mainServerDTO1.getId());
        assertThat(mainServerDTO1).isEqualTo(mainServerDTO2);
        mainServerDTO2.setId("id2");
        assertThat(mainServerDTO1).isNotEqualTo(mainServerDTO2);
        mainServerDTO1.setId(null);
        assertThat(mainServerDTO1).isNotEqualTo(mainServerDTO2);
    }
}
