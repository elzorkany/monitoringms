package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.MonitoringmsApp;
import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.repository.SubServerRepository;
import com.logicyel.monitoringms.repository.search.SubServerSearchRepository;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.service.mapper.SubServerMapper;
import com.logicyel.monitoringms.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;

import static com.logicyel.monitoringms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubServerResource REST controller.
 *
 * @see SubServerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringmsApp.class)
public class SubServerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_URL = "AAAAAAAAAA";
    private static final String UPDATED_BASE_URL = "BBBBBBBBBB";

    private static final int DEFAULT_ID_ON_SERVER = 1111;
    private static final int UPDATED_ID_ON_SERVER = 2222;

    @Autowired
    private SubServerRepository subServerRepository;

    @Autowired
    private SubServerMapper subServerMapper;

    @Autowired
    private SubServerService subServerService;

    @Autowired
    private MainServerService mainServerService;
    /**
     * This repository is mocked in the com.logicyel.monitoringms.repository.search TriggerQuartzStream package.
     *
     * @see com.logicyel.monitoringms.repository.search.OriginServerSearchRepositoryMockConfiguration
     */
    @Autowired
    private SubServerSearchRepository mockSubServerSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;


    private MockMvc restSubServerMockMvc;

    private SubServer subServer;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubServerResource subServerResource = new SubServerResource(subServerService, mainServerService);
        this.restSubServerMockMvc = MockMvcBuilders.standaloneSetup(subServerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this TriggerQuartzStream.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they TriggerQuartzStream an entity which requires the current entity.
     */
    public static SubServer createEntity() {
        SubServer subServer = new SubServer()
            .name(DEFAULT_NAME)
            .baseUrl(DEFAULT_BASE_URL)
            .idOnServer(DEFAULT_ID_ON_SERVER);
        return subServer;
    }

    @Before
    public void initTest() {
        subServerRepository.deleteAll();
        subServer = createEntity();
    }

    @Test
    public void createSubServer() throws Exception {
//        int databaseSizeBeforeCreate = subServerRepository.findAll().size();
//
//        // Create the SubServer
//        SubServerDTO subServerDTO = subServerMapper.toDto(subServer);
//        restSubServerMockMvc.perform(post("/api/sub-servers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(subServerDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the SubServer in the database
//        List<SubServer> subServerList = subServerRepository.findAll();
//        assertThat(subServerList).hasSize(databaseSizeBeforeCreate + 1);
//        SubServer testSubServer = subServerList.get(subServerList.size() - 1);
//        assertThat(testSubServer.getName()).isEqualTo(DEFAULT_NAME);
//        assertThat(testSubServer.getBaseUrl()).isEqualTo(DEFAULT_BASE_URL);
//        assertThat(testSubServer.getIdOnServer()).isEqualTo(DEFAULT_ID_ON_SERVER);
//
//        // Validate the SubServer in Elasticsearch
//        verify(mockSubServerSearchRepository, times(1)).save(testSubServer);
    }

    @Test
    public void createSubServerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subServerRepository.findAll().size();

        // Create the SubServer with an existing ID
        subServer.setId("existing_id");
        SubServerDTO subServerDTO = subServerMapper.toDto(subServer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubServerMockMvc.perform(post("/api/sub-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subServerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubServer in the database
        List<SubServer> subServerList = subServerRepository.findAll();
        assertThat(subServerList).hasSize(databaseSizeBeforeCreate);

        // Validate the SubServer in Elasticsearch
        verify(mockSubServerSearchRepository, times(0)).save(subServer);
    }

    @Test
    public void getAllSubServers() throws Exception {
        // Initialize the database
        subServerRepository.save(subServer);

        // Get all the subServerList
        restSubServerMockMvc.perform(get("/api/sub-servers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subServer.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].baseUrl").value(hasItem(DEFAULT_BASE_URL.toString())))
            .andExpect(jsonPath("$.[*].idOnServer").value(hasItem(DEFAULT_ID_ON_SERVER)));
    }
    
    @Test
    public void getSubServer() throws Exception {
        // Initialize the database
        subServerRepository.save(subServer);

        // Get the subServer
        restSubServerMockMvc.perform(get("/api/sub-servers/{id}", subServer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subServer.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.baseUrl").value(DEFAULT_BASE_URL.toString()))
            .andExpect(jsonPath("$.idOnServer").value(DEFAULT_ID_ON_SERVER));

    }

    @Test
    public void getNonExistingSubServer() throws Exception {
        // Get the subServer
        restSubServerMockMvc.perform(get("/api/sub-servers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSubServer() throws Exception {
        // Initialize the database
        subServerRepository.save(subServer);

        int databaseSizeBeforeUpdate = subServerRepository.findAll().size();

        // Update the subServer
        SubServer updatedSubServer = subServerRepository.findById(subServer.getId()).get();
        updatedSubServer
            .name(UPDATED_NAME)
            .baseUrl(UPDATED_BASE_URL)
            .idOnServer(UPDATED_ID_ON_SERVER);
        SubServerDTO subServerDTO = subServerMapper.toDto(updatedSubServer);

        restSubServerMockMvc.perform(put("/api/sub-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subServerDTO)))
            .andExpect(status().isOk());

        // Validate the SubServer in the database
        List<SubServer> subServerList = subServerRepository.findAll();
        assertThat(subServerList).hasSize(databaseSizeBeforeUpdate);
        SubServer testSubServer = subServerList.get(subServerList.size() - 1);
        assertThat(testSubServer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSubServer.getBaseUrl()).isEqualTo(UPDATED_BASE_URL);
        assertThat(testSubServer.getIdOnServer()).isEqualTo(UPDATED_ID_ON_SERVER);

        // Validate the SubServer in Elasticsearch
        verify(mockSubServerSearchRepository, times(1)).save(testSubServer);
    }

    @Test
    public void updateNonExistingSubServer() throws Exception {
        int databaseSizeBeforeUpdate = subServerRepository.findAll().size();

        // Create the SubServer
        SubServerDTO subServerDTO = subServerMapper.toDto(subServer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubServerMockMvc.perform(put("/api/sub-servers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subServerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubServer in the database
        List<SubServer> subServerList = subServerRepository.findAll();
        assertThat(subServerList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SubServer in Elasticsearch
        verify(mockSubServerSearchRepository, times(0)).save(subServer);
    }

    @Test
    public void deleteSubServer() throws Exception {
        // Initialize the database
        subServerRepository.save(subServer);

        int databaseSizeBeforeDelete = subServerRepository.findAll().size();

        // Delete the subServer
        restSubServerMockMvc.perform(delete("/api/sub-servers/{id}", subServer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubServer> subServerList = subServerRepository.findAll();
        assertThat(subServerList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the SubServer in Elasticsearch
        verify(mockSubServerSearchRepository, times(1)).deleteById(subServer.getId());
    }

    @Test
    public void searchSubServer() throws Exception {
        // Initialize the database
        subServerRepository.save(subServer);
        when(mockSubServerSearchRepository.search(queryStringQuery("id:" + subServer.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(subServer), PageRequest.of(0, 1), 1));
        // Search the subServer
        restSubServerMockMvc.perform(get("/api/_search/sub-servers?query=id:" + subServer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subServer.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].baseUrl").value(hasItem(DEFAULT_BASE_URL)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubServer.class);
        SubServer subServer1 = new SubServer();
        subServer1.setName("name1");
        subServer1.setBaseUrl("url1");
        SubServer subServer2 = new SubServer();
        subServer2.setIdOnServer(subServer1.getIdOnServer());
        subServer2.setBaseUrl(subServer1.getBaseUrl());
        subServer2.setName(subServer1.getName());
        assertThat(subServer1).isEqualTo(subServer2);
        subServer2.setName("name2");
        subServer2.setBaseUrl("url2");
        assertThat(subServer1).isNotEqualTo(subServer2);
        subServer1.setName(null);
        subServer1.setBaseUrl(null);
        assertThat(subServer1).isNotEqualTo(subServer2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubServerDTO.class);
        SubServerDTO subServerDTO1 = new SubServerDTO();
        subServerDTO1.setName("name1");
        subServerDTO1.setBaseUrl("url1");
        SubServerDTO subServerDTO2 = new SubServerDTO();
        assertThat(subServerDTO1).isNotEqualTo(subServerDTO2);
        subServerDTO2.setIdOnServer(subServerDTO1.getIdOnServer());
        subServerDTO2.setBaseUrl(subServerDTO1.getBaseUrl());
        subServerDTO2.setName(subServerDTO1.getName());
        assertThat(subServerDTO1).isEqualTo(subServerDTO2);
        subServerDTO2.setName("name2");
        subServerDTO2.setBaseUrl("url2");
        assertThat(subServerDTO1).isNotEqualTo(subServerDTO2);
        subServerDTO1.setName(null);
        subServerDTO1.setBaseUrl(null);
        assertThat(subServerDTO1).isNotEqualTo(subServerDTO2);
    }
}
