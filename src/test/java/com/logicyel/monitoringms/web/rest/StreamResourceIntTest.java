package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.MonitoringmsApp;
import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.repository.StreamRepository;
import com.logicyel.monitoringms.repository.search.StreamSearchRepository;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.mapper.StreamMapper;
import com.logicyel.monitoringms.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;

import static com.logicyel.monitoringms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StreamResource REST controller.
 *
 * @see StreamResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringmsApp.class)
public class StreamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final int DEFAULT_ID_ON_SERVER = 11111;
    private static final int UPDATED_ID_ON_SERVER = 22222;

    @Autowired
    private StreamRepository streamRepository;

    @Autowired
    private StreamMapper streamMapper;

    @Autowired
    private StreamService streamService;

    /**
     * This repository is mocked in the com.logicyel.monitoringms.repository.search TriggerQuartzStream package.
     *
     * @see com.logicyel.monitoringms.repository.search.StreamSearchRepositoryMockConfiguration
     */
    @Autowired
    private StreamSearchRepository mockStreamSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    @Autowired
    private SubServerService subServerService;

    private MockMvc restStreamMockMvc;

    private Stream stream;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StreamResource streamResource = new StreamResource(streamService, subServerService);
        this.restStreamMockMvc = MockMvcBuilders.standaloneSetup(streamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this TriggerQuartzStream.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they TriggerQuartzStream an entity which requires the current entity.
     */
    public static Stream createEntity() {
        Stream stream = new Stream()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL)
            .idOnServer(DEFAULT_ID_ON_SERVER);
        return stream;
    }

    @Before
    public void initTest() {
        streamRepository.deleteAll();
        stream = createEntity();
    }

    @Test
    public void createStream() throws Exception {
        int databaseSizeBeforeCreate = streamRepository.findAll().size();

        // Create the Stream
        StreamDTO streamDTO = streamMapper.toDto(stream);
        restStreamMockMvc.perform(post("/api/streams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(streamDTO)))
            .andExpect(status().isCreated());

        // Validate the Stream in the database
        List<Stream> streamList = streamRepository.findAll();
        assertThat(streamList).hasSize(databaseSizeBeforeCreate + 1);
        Stream testStream = streamList.get(streamList.size() - 1);
        assertThat(testStream.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStream.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testStream.getIdOnServer()).isEqualTo(DEFAULT_ID_ON_SERVER);

        // Validate the Stream in Elasticsearch
        verify(mockStreamSearchRepository, times(1)).save(testStream);
    }

    @Test
    public void createStreamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = streamRepository.findAll().size();

        // Create the Stream with an existing ID
        stream.setId("existing_id");
        StreamDTO streamDTO = streamMapper.toDto(stream);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStreamMockMvc.perform(post("/api/streams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(streamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Stream in the database
        List<Stream> streamList = streamRepository.findAll();
        assertThat(streamList).hasSize(databaseSizeBeforeCreate);

        // Validate the Stream in Elasticsearch
        verify(mockStreamSearchRepository, times(0)).save(stream);
    }

    @Test
    public void getAllStreams() throws Exception {
        // Initialize the database
        streamRepository.save(stream);

        // Get all the streamList
        restStreamMockMvc.perform(get("/api/streams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stream.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].idOnServer").value(hasItem(DEFAULT_ID_ON_SERVER)));
    }

    @Test
    public void getStream() throws Exception {
        // Initialize the database
        streamRepository.save(stream);

        // Get the stream
        restStreamMockMvc.perform(get("/api/streams/{id}", stream.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(stream.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.idOnServer").value(DEFAULT_ID_ON_SERVER));
    }

    @Test
    public void getNonExistingStream() throws Exception {
        // Get the stream
        restStreamMockMvc.perform(get("/api/streams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateStream() throws Exception {
        // Initialize the database
        streamRepository.save(stream);

        int databaseSizeBeforeUpdate = streamRepository.findAll().size();

        // Update the stream
        Stream updatedStream = streamRepository.findById(stream.getId()).get();
        updatedStream
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .idOnServer(UPDATED_ID_ON_SERVER);
        StreamDTO streamDTO = streamMapper.toDto(updatedStream);

        restStreamMockMvc.perform(put("/api/streams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(streamDTO)))
            .andExpect(status().isOk());

        // Validate the Stream in the database
        List<Stream> streamList = streamRepository.findAll();
        assertThat(streamList).hasSize(databaseSizeBeforeUpdate);
        Stream testStream = streamList.get(streamList.size() - 1);
        assertThat(testStream.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStream.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testStream.getIdOnServer()).isEqualTo(UPDATED_ID_ON_SERVER);

        // Validate the Stream in Elasticsearch
        verify(mockStreamSearchRepository, times(1)).save(testStream);
    }

    @Test
    public void updateNonExistingStream() throws Exception {
        int databaseSizeBeforeUpdate = streamRepository.findAll().size();

        // Create the Stream
        StreamDTO streamDTO = streamMapper.toDto(stream);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStreamMockMvc.perform(put("/api/streams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(streamDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Stream in the database
        List<Stream> streamList = streamRepository.findAll();
        assertThat(streamList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Stream in Elasticsearch
        verify(mockStreamSearchRepository, times(0)).save(stream);
    }

    @Test
    public void deleteStream() throws Exception {
        // Initialize the database
        streamRepository.save(stream);

        int databaseSizeBeforeDelete = streamRepository.findAll().size();

        // Delete the stream
        restStreamMockMvc.perform(delete("/api/streams/{id}", stream.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Stream> streamList = streamRepository.findAll();
        assertThat(streamList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Stream in Elasticsearch
        verify(mockStreamSearchRepository, times(1)).deleteById(stream.getId());
    }

    @Test
    public void searchStream() throws Exception {
        // Initialize the database
        streamRepository.save(stream);
        when(mockStreamSearchRepository.search(queryStringQuery("id:" + stream.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(stream), PageRequest.of(0, 1), 1));
        // Search the stream
        restStreamMockMvc.perform(get("/api/_search/streams?query=id:" + stream.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stream.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].idOnServer").value(hasItem(DEFAULT_ID_ON_SERVER)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Stream.class);
        Stream stream1 = new Stream();
        stream1.setUrl("url1");
        stream1.setName("name1");
        Stream stream2 = new Stream();
        stream2.setUrl(stream1.getUrl());
        stream2.setName(stream1.getName());
        stream2.setIdOnServer(stream1.getIdOnServer());
        assertThat(stream1).isEqualTo(stream2);
        stream1.setUrl("url2");
        stream1.setName("name2");
        assertThat(stream1).isNotEqualTo(stream2);
        stream1.setUrl(null);
        stream1.setName(null);
        assertThat(stream1).isNotEqualTo(stream2);

    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StreamDTO.class);
        StreamDTO streamDTO1 = new StreamDTO();
        streamDTO1.setUrl("url1");
        streamDTO1.setName("name1");
        StreamDTO streamDTO2 = new StreamDTO();
        assertThat(streamDTO1).isNotEqualTo(streamDTO2);
        streamDTO2.setUrl(streamDTO1.getUrl());
        streamDTO2.setName(streamDTO1.getName());
        streamDTO2.setIdOnServer(streamDTO1.getIdOnServer());
        assertThat(streamDTO1).isEqualTo(streamDTO2);
        streamDTO1.setUrl("url2");
        streamDTO1.setName("name2");
        assertThat(streamDTO1).isNotEqualTo(streamDTO2);
        streamDTO1.setUrl(null);
        streamDTO1.setName(null);
        assertThat(streamDTO1).isNotEqualTo(streamDTO2);
    }
}
