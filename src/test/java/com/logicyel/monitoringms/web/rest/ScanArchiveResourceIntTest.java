package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.MonitoringmsApp;

import com.logicyel.monitoringms.domain.ScanArchive;
import com.logicyel.monitoringms.repository.ScanArchiveRepository;
import com.logicyel.monitoringms.repository.search.ScanArchiveSearchRepository;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;
import com.logicyel.monitoringms.service.mapper.ScanArchiveMapper;
import com.logicyel.monitoringms.web.rest.errors.ExceptionTranslator;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;


import static com.logicyel.monitoringms.web.rest.TestUtil.sameInstant;
import static com.logicyel.monitoringms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ScanArchiveResource REST controller.
 *
 * @see ScanArchiveResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringmsApp.class)
public class ScanArchiveResourceIntTest {

//    private static final DateTime DEFAULT_SCAN_TIME = new DateTime();
//    private static final DateTime UPDATED_SCAN_TIME = new DateTime();

    private static final String DEFAULT_PREVIOUS_STATE = "AAAAAAAAAA";
    private static final String UPDATED_PREVIOUS_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_STATE = "BBBBBBBBBB";

    @Autowired
    private ScanArchiveRepository scanArchiveRepository;

    @Autowired
    private ScanArchiveMapper scanArchiveMapper;

    @Autowired
    private ScanArchiveService scanArchiveService;

    /**
     * This repository is mocked in the com.logicyel.monitoringms.repository.search test package.
     *
     * @see com.logicyel.monitoringms.repository.search.ScanArchiveSearchRepositoryMockConfiguration
     */
    @Autowired
    private ScanArchiveSearchRepository mockScanArchiveSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restScanArchiveMockMvc;

    private ScanArchive scanArchive;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ScanArchiveResource scanArchiveResource = new ScanArchiveResource(scanArchiveService);
        this.restScanArchiveMockMvc = MockMvcBuilders.standaloneSetup(scanArchiveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ScanArchive createEntity() {
        ScanArchive scanArchive = new ScanArchive()
            .previousState(DEFAULT_PREVIOUS_STATE)
            .currentState(DEFAULT_CURRENT_STATE);
        return scanArchive;
    }

    @Before
    public void initTest() {
        scanArchiveRepository.deleteAll();
        scanArchive = createEntity();
    }

    @Test
    public void createScanArchive() throws Exception {
        int databaseSizeBeforeCreate = scanArchiveRepository.findAll().size();

        // Create the ScanArchive
        ScanArchiveDTO scanArchiveDTO = scanArchiveMapper.toDto(scanArchive);
        restScanArchiveMockMvc.perform(post("/api/scan-archives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scanArchiveDTO)))
            .andExpect(status().isCreated());

        // Validate the ScanArchive in the database
        List<ScanArchive> scanArchiveList = scanArchiveRepository.findAll();
        assertThat(scanArchiveList).hasSize(databaseSizeBeforeCreate + 1);
        ScanArchive testScanArchive = scanArchiveList.get(scanArchiveList.size() - 1);
        assertThat(testScanArchive.getPreviousState()).isEqualTo(DEFAULT_PREVIOUS_STATE);
        assertThat(testScanArchive.getCurrentState()).isEqualTo(DEFAULT_CURRENT_STATE);

        // Validate the ScanArchive in Elasticsearch
        verify(mockScanArchiveSearchRepository, times(1)).save(testScanArchive);
    }

    @Test
    public void createScanArchiveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = scanArchiveRepository.findAll().size();

        // Create the ScanArchive with an existing ID
        scanArchive.setId("existing_id");
        ScanArchiveDTO scanArchiveDTO = scanArchiveMapper.toDto(scanArchive);

        // An entity with an existing ID cannot be created, so this API call must fail
        restScanArchiveMockMvc.perform(post("/api/scan-archives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scanArchiveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ScanArchive in the database
        List<ScanArchive> scanArchiveList = scanArchiveRepository.findAll();
        assertThat(scanArchiveList).hasSize(databaseSizeBeforeCreate);

        // Validate the ScanArchive in Elasticsearch
        verify(mockScanArchiveSearchRepository, times(0)).save(scanArchive);
    }

    @Test
    public void getAllScanArchives() throws Exception {
        // Initialize the database
        scanArchiveRepository.save(scanArchive);

        // Get all the scanArchiveList
        restScanArchiveMockMvc.perform(get("/api/scan-archives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(scanArchive.getId())))
            .andExpect(jsonPath("$.[*].previousState").value(hasItem(DEFAULT_PREVIOUS_STATE.toString())))
            .andExpect(jsonPath("$.[*].currentState").value(hasItem(DEFAULT_CURRENT_STATE.toString())));
    }

    @Test
    public void getScanArchive() throws Exception {
        // Initialize the database
        scanArchiveRepository.save(scanArchive);

        // Get the scanArchive
        restScanArchiveMockMvc.perform(get("/api/scan-archives/{id}", scanArchive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(scanArchive.getId()))
            .andExpect(jsonPath("$.previousState").value(DEFAULT_PREVIOUS_STATE.toString()))
            .andExpect(jsonPath("$.currentState").value(DEFAULT_CURRENT_STATE.toString()));
    }

    @Test
    public void getNonExistingScanArchive() throws Exception {
        // Get the scanArchive
        restScanArchiveMockMvc.perform(get("/api/scan-archives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateScanArchive() throws Exception {
        // Initialize the database
        scanArchiveRepository.save(scanArchive);

        int databaseSizeBeforeUpdate = scanArchiveRepository.findAll().size();

        // Update the scanArchive
        ScanArchive updatedScanArchive = scanArchiveRepository.findById(scanArchive.getId()).get();
        updatedScanArchive
            .previousState(UPDATED_PREVIOUS_STATE)
            .currentState(UPDATED_CURRENT_STATE);
        ScanArchiveDTO scanArchiveDTO = scanArchiveMapper.toDto(updatedScanArchive);

        restScanArchiveMockMvc.perform(put("/api/scan-archives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scanArchiveDTO)))
            .andExpect(status().isOk());

        // Validate the ScanArchive in the database
        List<ScanArchive> scanArchiveList = scanArchiveRepository.findAll();
        assertThat(scanArchiveList).hasSize(databaseSizeBeforeUpdate);
        ScanArchive testScanArchive = scanArchiveList.get(scanArchiveList.size() - 1);
        assertThat(testScanArchive.getPreviousState()).isEqualTo(UPDATED_PREVIOUS_STATE);
        assertThat(testScanArchive.getCurrentState()).isEqualTo(UPDATED_CURRENT_STATE);

        // Validate the ScanArchive in Elasticsearch
        verify(mockScanArchiveSearchRepository, times(1)).save(testScanArchive);
    }

    @Test
    public void updateNonExistingScanArchive() throws Exception {
        int databaseSizeBeforeUpdate = scanArchiveRepository.findAll().size();

        // Create the ScanArchive
        ScanArchiveDTO scanArchiveDTO = scanArchiveMapper.toDto(scanArchive);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restScanArchiveMockMvc.perform(put("/api/scan-archives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(scanArchiveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ScanArchive in the database
        List<ScanArchive> scanArchiveList = scanArchiveRepository.findAll();
        assertThat(scanArchiveList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ScanArchive in Elasticsearch
        verify(mockScanArchiveSearchRepository, times(0)).save(scanArchive);
    }

    @Test
    public void deleteScanArchive() throws Exception {
        // Initialize the database
        scanArchiveRepository.save(scanArchive);

        int databaseSizeBeforeDelete = scanArchiveRepository.findAll().size();

        // Delete the scanArchive
        restScanArchiveMockMvc.perform(delete("/api/scan-archives/{id}", scanArchive.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ScanArchive> scanArchiveList = scanArchiveRepository.findAll();
        assertThat(scanArchiveList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ScanArchive in Elasticsearch
        verify(mockScanArchiveSearchRepository, times(1)).deleteById(scanArchive.getId());
    }

    @Test
    public void searchScanArchive() throws Exception {
        // Initialize the database
        scanArchiveRepository.save(scanArchive);
        when(mockScanArchiveSearchRepository.search(queryStringQuery("id:" + scanArchive.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(scanArchive), PageRequest.of(0, 1), 1));
        // Search the scanArchive
        restScanArchiveMockMvc.perform(get("/api/_search/scan-archives?query=id:" + scanArchive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(scanArchive.getId())))
            .andExpect(jsonPath("$.[*].previousState").value(hasItem(DEFAULT_PREVIOUS_STATE)))
            .andExpect(jsonPath("$.[*].currentState").value(hasItem(DEFAULT_CURRENT_STATE)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ScanArchive.class);
        ScanArchive scanArchive1 = new ScanArchive();
        scanArchive1.setId("id1");
        ScanArchive scanArchive2 = new ScanArchive();
        scanArchive2.setId(scanArchive1.getId());
        assertThat(scanArchive1).isEqualTo(scanArchive2);
        scanArchive2.setId("id2");
        assertThat(scanArchive1).isNotEqualTo(scanArchive2);
        scanArchive1.setId(null);
        assertThat(scanArchive1).isNotEqualTo(scanArchive2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ScanArchiveDTO.class);
        ScanArchiveDTO scanArchiveDTO1 = new ScanArchiveDTO();
        scanArchiveDTO1.setId("id1");
        ScanArchiveDTO scanArchiveDTO2 = new ScanArchiveDTO();
        assertThat(scanArchiveDTO1).isNotEqualTo(scanArchiveDTO2);
        scanArchiveDTO2.setId(scanArchiveDTO1.getId());
        assertThat(scanArchiveDTO1).isEqualTo(scanArchiveDTO2);
        scanArchiveDTO2.setId("id2");
        assertThat(scanArchiveDTO1).isNotEqualTo(scanArchiveDTO2);
        scanArchiveDTO1.setId(null);
        assertThat(scanArchiveDTO1).isNotEqualTo(scanArchiveDTO2);
    }
}
