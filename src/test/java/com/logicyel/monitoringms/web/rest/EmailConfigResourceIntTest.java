package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.MonitoringmsApp;

import com.logicyel.monitoringms.domain.EmailConfig;
import com.logicyel.monitoringms.repository.EmailConfigRepository;
import com.logicyel.monitoringms.repository.search.EmailConfigSearchRepository;
import com.logicyel.monitoringms.service.EmailConfigService;
import com.logicyel.monitoringms.service.dto.EmailConfigDTO;
import com.logicyel.monitoringms.service.mapper.EmailConfigMapper;
import com.logicyel.monitoringms.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.Collections;
import java.util.List;


import static com.logicyel.monitoringms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EmailConfigResource REST controller.
 *
 * @see EmailConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringmsApp.class)
public class EmailConfigResourceIntTest {

    private static final String DEFAULT_HOST = "AAAAAAAAAA";
    private static final String UPDATED_HOST = "BBBBBBBBBB";

    private static final Integer DEFAULT_PORT = 1;
    private static final Integer UPDATED_PORT = 2;

    private static final String DEFAULT_PROTOCOL = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private EmailConfigRepository emailConfigRepository;

    @Autowired
    private EmailConfigMapper emailConfigMapper;

    @Autowired
    private EmailConfigService emailConfigService;

    /**
     * This repository is mocked in the com.logicyel.monitoringms.repository.search test package.
     *
     * @see com.logicyel.monitoringms.repository.search.EmailConfigSearchRepositoryMockConfiguration
     */
    @Autowired
    private EmailConfigSearchRepository mockEmailConfigSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restEmailConfigMockMvc;

    private EmailConfig emailConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmailConfigResource emailConfigResource = new EmailConfigResource(emailConfigService);
        this.restEmailConfigMockMvc = MockMvcBuilders.standaloneSetup(emailConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailConfig createEntity() {
        EmailConfig emailConfig = new EmailConfig()
            .host(DEFAULT_HOST)
            .port(DEFAULT_PORT)
            .protocol(DEFAULT_PROTOCOL)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD);
        return emailConfig;
    }

    @Before
    public void initTest() {
        emailConfigRepository.deleteAll();
        emailConfig = createEntity();
    }

    @Test
    public void createEmailConfig() throws Exception {
        int databaseSizeBeforeCreate = emailConfigRepository.findAll().size();

        // Create the EmailConfig
        EmailConfigDTO emailConfigDTO = emailConfigMapper.toDto(emailConfig);
        restEmailConfigMockMvc.perform(post("/api/email-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the EmailConfig in the database
        List<EmailConfig> emailConfigList = emailConfigRepository.findAll();
        assertThat(emailConfigList).hasSize(databaseSizeBeforeCreate + 1);
        EmailConfig testEmailConfig = emailConfigList.get(emailConfigList.size() - 1);
        assertThat(testEmailConfig.getHost()).isEqualTo(DEFAULT_HOST);
        assertThat(testEmailConfig.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testEmailConfig.getProtocol()).isEqualTo(DEFAULT_PROTOCOL);
        assertThat(testEmailConfig.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testEmailConfig.getPassword()).isEqualTo(DEFAULT_PASSWORD);

        // Validate the EmailConfig in Elasticsearch
        verify(mockEmailConfigSearchRepository, times(1)).save(testEmailConfig);
    }

    @Test
    public void createEmailConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = emailConfigRepository.findAll().size();

        // Create the EmailConfig with an existing ID
        emailConfig.setId("existing_id");
        EmailConfigDTO emailConfigDTO = emailConfigMapper.toDto(emailConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmailConfigMockMvc.perform(post("/api/email-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmailConfig in the database
        List<EmailConfig> emailConfigList = emailConfigRepository.findAll();
        assertThat(emailConfigList).hasSize(databaseSizeBeforeCreate);

        // Validate the EmailConfig in Elasticsearch
        verify(mockEmailConfigSearchRepository, times(0)).save(emailConfig);
    }

    @Test
    public void getAllEmailConfigs() throws Exception {
        // Initialize the database
        emailConfigRepository.save(emailConfig);

        // Get all the emailConfigList
        restEmailConfigMockMvc.perform(get("/api/email-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailConfig.getId())))
            .andExpect(jsonPath("$.[*].host").value(hasItem(DEFAULT_HOST.toString())))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(DEFAULT_PROTOCOL.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())));
    }
    
    @Test
    public void getEmailConfig() throws Exception {
        // Initialize the database
        emailConfigRepository.save(emailConfig);

        // Get the emailConfig
        restEmailConfigMockMvc.perform(get("/api/email-configs/{id}", emailConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(emailConfig.getId()))
            .andExpect(jsonPath("$.host").value(DEFAULT_HOST.toString()))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT))
            .andExpect(jsonPath("$.protocol").value(DEFAULT_PROTOCOL.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()));
    }

    @Test
    public void getNonExistingEmailConfig() throws Exception {
        // Get the emailConfig
        restEmailConfigMockMvc.perform(get("/api/email-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateEmailConfig() throws Exception {
        // Initialize the database
        emailConfigRepository.save(emailConfig);

        int databaseSizeBeforeUpdate = emailConfigRepository.findAll().size();

        // Update the emailConfig
        EmailConfig updatedEmailConfig = emailConfigRepository.findById(emailConfig.getId()).get();
        updatedEmailConfig
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .protocol(UPDATED_PROTOCOL)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD);
        EmailConfigDTO emailConfigDTO = emailConfigMapper.toDto(updatedEmailConfig);

        restEmailConfigMockMvc.perform(put("/api/email-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailConfigDTO)))
            .andExpect(status().isOk());

        // Validate the EmailConfig in the database
        List<EmailConfig> emailConfigList = emailConfigRepository.findAll();
        assertThat(emailConfigList).hasSize(databaseSizeBeforeUpdate);
        EmailConfig testEmailConfig = emailConfigList.get(emailConfigList.size() - 1);
        assertThat(testEmailConfig.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testEmailConfig.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testEmailConfig.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testEmailConfig.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testEmailConfig.getPassword()).isEqualTo(UPDATED_PASSWORD);

        // Validate the EmailConfig in Elasticsearch
        verify(mockEmailConfigSearchRepository, times(1)).save(testEmailConfig);
    }

    @Test
    public void updateNonExistingEmailConfig() throws Exception {
        int databaseSizeBeforeUpdate = emailConfigRepository.findAll().size();

        // Create the EmailConfig
        EmailConfigDTO emailConfigDTO = emailConfigMapper.toDto(emailConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmailConfigMockMvc.perform(put("/api/email-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(emailConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmailConfig in the database
        List<EmailConfig> emailConfigList = emailConfigRepository.findAll();
        assertThat(emailConfigList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EmailConfig in Elasticsearch
        verify(mockEmailConfigSearchRepository, times(0)).save(emailConfig);
    }

    @Test
    public void deleteEmailConfig() throws Exception {
        // Initialize the database
        emailConfigRepository.save(emailConfig);

        int databaseSizeBeforeDelete = emailConfigRepository.findAll().size();

        // Delete the emailConfig
        restEmailConfigMockMvc.perform(delete("/api/email-configs/{id}", emailConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EmailConfig> emailConfigList = emailConfigRepository.findAll();
        assertThat(emailConfigList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EmailConfig in Elasticsearch
        verify(mockEmailConfigSearchRepository, times(1)).deleteById(emailConfig.getId());
    }

    @Test
    public void searchEmailConfig() throws Exception {
        // Initialize the database
        emailConfigRepository.save(emailConfig);
        when(mockEmailConfigSearchRepository.search(queryStringQuery("id:" + emailConfig.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(emailConfig), PageRequest.of(0, 1), 1));
        // Search the emailConfig
        restEmailConfigMockMvc.perform(get("/api/_search/email-configs?query=id:" + emailConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailConfig.getId())))
            .andExpect(jsonPath("$.[*].host").value(hasItem(DEFAULT_HOST)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(DEFAULT_PROTOCOL)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailConfig.class);
        EmailConfig emailConfig1 = new EmailConfig();
        emailConfig1.setId("id1");
        EmailConfig emailConfig2 = new EmailConfig();
        emailConfig2.setId(emailConfig1.getId());
        assertThat(emailConfig1).isEqualTo(emailConfig2);
        emailConfig2.setId("id2");
        assertThat(emailConfig1).isNotEqualTo(emailConfig2);
        emailConfig1.setId(null);
        assertThat(emailConfig1).isNotEqualTo(emailConfig2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailConfigDTO.class);
        EmailConfigDTO emailConfigDTO1 = new EmailConfigDTO();
        emailConfigDTO1.setId("id1");
        EmailConfigDTO emailConfigDTO2 = new EmailConfigDTO();
        assertThat(emailConfigDTO1).isNotEqualTo(emailConfigDTO2);
        emailConfigDTO2.setId(emailConfigDTO1.getId());
        assertThat(emailConfigDTO1).isEqualTo(emailConfigDTO2);
        emailConfigDTO2.setId("id2");
        assertThat(emailConfigDTO1).isNotEqualTo(emailConfigDTO2);
        emailConfigDTO1.setId(null);
        assertThat(emailConfigDTO1).isNotEqualTo(emailConfigDTO2);
    }
}
