package com.logicyel.monitoringms.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of SubServerSearchRepository to TriggerQuartzStream the
 * application without starting Elasticsearch.
 */
@Configuration
public class OriginServerSearchRepositoryMockConfiguration {

    @MockBean
    private SubServerSearchRepository mockSubServerSearchRepository;

}
