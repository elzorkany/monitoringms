package com.logicyel.monitoringms.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of EmailConfigSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class EmailConfigSearchRepositoryMockConfiguration {

    @MockBean
    private EmailConfigSearchRepository mockEmailConfigSearchRepository;

}
