package com.logicyel.monitoringms.storage;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface StorageService {

    File store(MultipartFile file);
}
