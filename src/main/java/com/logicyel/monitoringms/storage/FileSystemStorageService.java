package com.logicyel.monitoringms.storage;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileSystemStorageService implements StorageService {

    @Override
    public File store(MultipartFile file) {
        if (file == null || file.getOriginalFilename() == null)
            throw new StorageException("Failed to get file.");

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " +
                    filename);
            }
            Path rootLocation = Paths.get(StorageProperties.getLocation());
            if (!Files.isDirectory(rootLocation)) {
                Files.createDirectories(rootLocation);
            }
            Files.copy(file.getInputStream(), rootLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING);

            return new File(rootLocation.toString()+"/"+file.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
            throw new StorageException("Failed to store file " + filename, e);
        }
    }
}
