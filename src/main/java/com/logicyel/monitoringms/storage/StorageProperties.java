package com.logicyel.monitoringms.storage;

import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private final static String location = "resources/uploads/";

    public static String getLocation() {
        return location;
    }
}
