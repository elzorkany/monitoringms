package com.logicyel.monitoringms.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final  String ONLINE = "online";
    public static final  String OFFLINE = "offline";
    public static final  String DELETEJOBERROR = "can't delete the job ";
    public static final  String STARTJOBERROR = "can't strart the job ";

    private Constants() {
    }
}
