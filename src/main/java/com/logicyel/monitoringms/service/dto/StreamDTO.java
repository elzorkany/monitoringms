package com.logicyel.monitoringms.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Stream entity.
 */
public class StreamDTO implements Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    private String name;

    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int idOnServer;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long inherentInterval;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long customInterval;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private DateTime lastScanned;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String subServerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String inherentEmailTrigger;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customEmailTrigger;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> emailUserIds;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdOnServer() {
        return idOnServer;
    }

    public void setIdOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
    }

    public String getSubServerId() {
        return subServerId;
    }

    public void setSubServerId(String subServerId) {
        this.subServerId = subServerId;
    }

    public Long getInherentInterval() {
        return inherentInterval;
    }

    public void setInherentInterval(Long inherentInterval) {
        this.inherentInterval = inherentInterval;
    }

    public Long getCustomInterval() {
        return customInterval;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInherentEmailTrigger() {
        return inherentEmailTrigger;
    }

    public void setInherentEmailTrigger(String inherentEmailTrigger) {
        this.inherentEmailTrigger = inherentEmailTrigger;
    }

    public String getCustomEmailTrigger() {
        return customEmailTrigger;
    }

    public void setCustomEmailTrigger(String customEmailTrigger) {
        this.customEmailTrigger = customEmailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    public void setCustomInterval(Long customInterval) {
        this.customInterval = customInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamDTO stream = (StreamDTO) o;

        if (stream.getName() == null || getName() == null || stream.getUrl() == null || getUrl() == null) {
            return false;
        }
        return name.equals(stream.name) &&
            url.equals(stream.url);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, url, idOnServer);
    }

    @Override
    public String toString() {
        return "StreamDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", idOnServer='" + getIdOnServer() + "'" +
            ", subServer=" + getSubServerId() +
            "}";
    }
}
