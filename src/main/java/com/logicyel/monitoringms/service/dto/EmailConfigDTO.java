package com.logicyel.monitoringms.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EmailConfig entity.
 */
public class EmailConfigDTO implements Serializable {

    private String id;

    private String host;

    private Integer port;

    private String protocol;

    private String username;

    private String password;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailConfigDTO emailConfigDTO = (EmailConfigDTO) o;
        if (emailConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emailConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmailConfigDTO{" +
            "id=" + getId() +
            ", host='" + getHost() + "'" +
            ", port=" + getPort() +
            ", protocol='" + getProtocol() + "'" +
            ", username='" + getUsername() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }
}
