package com.logicyel.monitoringms.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the SubServer entity.
 */
public class SubServerDTO implements Serializable {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    private String name;

    private String baseUrl;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int idOnServer;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long inherentInterval;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long customInterval;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private DateTime lastScanned;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mainServerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String inherentEmailTrigger;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customEmailTrigger;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> emailUserIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getMainServerId() {
        return mainServerId;
    }

    public void setMainServerId(String mainServerId) {
        this.mainServerId = mainServerId;
    }

    public int getIdOnServer() {
        return idOnServer;
    }

    public void setIdOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
    }

    public Long getInherentInterval() {
        return inherentInterval;
    }

    public void setInherentInterval(Long inherentInterval) {
        this.inherentInterval = inherentInterval;
    }

    public Long getCustomInterval() {
        return customInterval;
    }

    public void setCustomInterval(Long customInterval) {
        this.customInterval = customInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    public String getInherentEmailTrigger() {
        return inherentEmailTrigger;
    }

    public void setInherentEmailTrigger(String inherentEmailTrigger) {
        this.inherentEmailTrigger = inherentEmailTrigger;
    }

    public String getCustomEmailTrigger() {
        return customEmailTrigger;
    }

    public void setCustomEmailTrigger(String customEmailTrigger) {
        this.customEmailTrigger = customEmailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubServerDTO that = (SubServerDTO) o;
        if (that.getName() == null || getName() == null || that.getBaseUrl() == null || getBaseUrl() == null) {
            return false;
        }
        return name.equals(that.name) &&
            baseUrl.equals(that.baseUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, baseUrl, idOnServer);
    }

    @Override
    public String toString() {
        return "SubServerDTO{" +
            "id=" + getId() +
            ", idOnServer=" + getIdOnServer() +
            ", name='" + getName() + "'" +
            ", baseUrl='" + getBaseUrl() + "'" +
            ", mainServer=" + getMainServerId() +
            "}";
    }

}
