package com.logicyel.monitoringms.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the mainServer entity.
 */
public class MainServerDTO implements Serializable {

    private String id;
    private String serverType;

    private String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String token;

    private String baseUrl;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long scanInterval;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private DateTime lastScanned;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String emailTrigger;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> emailUserIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public Long getScanInterval() {
        return scanInterval;
    }

    public void setScanInterval(Long scanInterval) {
        this.scanInterval = scanInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    public String getEmailTrigger() {
        return emailTrigger;
    }

    public void setEmailTrigger(String emailTrigger) {
        this.emailTrigger = emailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MainServerDTO mainServerDTO = (MainServerDTO) o;
        if (mainServerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mainServerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MainServerDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", token='" + getToken() + "'" +
            ", baseUrl='" + getBaseUrl() + "'" +
            ", serverType='" + getServerType() + "'" +
            "}";
    }
}
