package com.logicyel.monitoringms.service.dto;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the ScanArchive entity.
 */
public class ScanArchiveDTO implements Serializable {

    private String id;

    private DateTime scanTime;

    private String previousState;

    private String currentState;


    private String streamId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getScanTime() {
        return scanTime;
    }

    public void setScanTime(DateTime scanTime) {
        this.scanTime = scanTime;
    }

    public String getPreviousState() {
        return previousState;
    }

    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScanArchiveDTO scanArchiveDTO = (ScanArchiveDTO) o;
        if (scanArchiveDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scanArchiveDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ScanArchiveDTO{" +
            "id=" + getId() +
            ", scanTime='" + getScanTime() + "'" +
            ", previousState='" + getPreviousState() + "'" +
            ", currentState='" + getCurrentState() + "'" +
            ", streamLogs=" + getStreamId() +
            "}";
    }
}
