package com.logicyel.monitoringms.service.action;

public enum EmailTrigger {
    OFFLINE("offline"), ONLINE("online"), BOTH("both"), NO_ACTION("no_action");

    private String value;

    EmailTrigger(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
