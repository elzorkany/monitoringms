package com.logicyel.monitoringms.service.mapper;

import com.logicyel.monitoringms.domain.*;
import com.logicyel.monitoringms.service.dto.EmailConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EmailConfig and its DTO EmailConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmailConfigMapper extends EntityMapper<EmailConfigDTO, EmailConfig> {



    default EmailConfig fromId(String id) {
        if (id == null) {
            return null;
        }
        EmailConfig emailConfig = new EmailConfig();
        emailConfig.setId(id);
        return emailConfig;
    }
}
