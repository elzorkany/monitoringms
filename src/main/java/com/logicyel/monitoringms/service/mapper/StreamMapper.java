package com.logicyel.monitoringms.service.mapper;

import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Stream and its DTO StreamDTO.
 */
@Mapper(componentModel = "spring", uses = {SubServerMapper.class})
public interface StreamMapper extends EntityMapper<StreamDTO, Stream> {

    @Mapping(source = "subServer.id", target = "subServerId")
    StreamDTO toDto(Stream stream);

    @Mapping(source = "subServerId", target = "subServer")
    @Mapping(target = "scanArchives", ignore = true)
    Stream toEntity(StreamDTO streamDTO);

    default Stream fromId(String id) {
        if (id == null) {
            return null;
        }
        Stream stream = new Stream();
        stream.setId(id);
        return stream;
    }
}
