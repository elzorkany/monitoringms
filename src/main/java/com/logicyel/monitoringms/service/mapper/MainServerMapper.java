package com.logicyel.monitoringms.service.mapper;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity mainServer and its DTO MainServerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MainServerMapper extends EntityMapper<MainServerDTO, MainServer> {


    @Mapping(target = "subServers", ignore = true)
    MainServer toEntity(MainServerDTO mainServerDTO);

    default MainServer fromId(String id) {
        if (id == null) {
            return null;
        }
        MainServer mainServer = new MainServer();
        mainServer.setId(id);
        return mainServer;
    }
}
