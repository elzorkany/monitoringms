package com.logicyel.monitoringms.service.mapper;

import com.logicyel.monitoringms.domain.*;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ScanArchive and its DTO ScanArchiveDTO.
 */
@Mapper(componentModel = "spring", uses = {StreamMapper.class})
public interface ScanArchiveMapper extends EntityMapper<ScanArchiveDTO, ScanArchive> {

    @Mapping(source = "stream.id", target = "streamId")
    ScanArchiveDTO toDto(ScanArchive scanArchive);

    @Mapping(source = "streamId", target = "stream")
    ScanArchive toEntity(ScanArchiveDTO scanArchiveDTO);

    default ScanArchive fromId(String id) {
        if (id == null) {
            return null;
        }
        ScanArchive scanArchive = new ScanArchive();
        scanArchive.setId(id);
        return scanArchive;
    }
}
