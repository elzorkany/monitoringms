package com.logicyel.monitoringms.service.mapper;

import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity SubServer and its DTO SubServerDTO.
 */
@Mapper(componentModel = "spring", uses = {MainServerMapper.class})
public interface SubServerMapper extends EntityMapper<SubServerDTO, SubServer> {

    @Mapping(source = "mainServer.id", target = "mainServerId")
    SubServerDTO toDto(SubServer subServer);

    @Mapping(source = "mainServerId", target = "mainServer")
    @Mapping(target = "streams", ignore = true)
    SubServer toEntity(SubServerDTO subServerDTO);

    default SubServer fromId(String id) {
        if (id == null) {
            return null;
        }
        SubServer subServer = new SubServer();
        subServer.setId(id);
        return subServer;
    }
}
