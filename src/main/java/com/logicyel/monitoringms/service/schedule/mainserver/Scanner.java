package com.logicyel.monitoringms.service.schedule.mainserver;

import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Scanner {

    private final MainServerService mainServerService;
    private final TriggerQuartzMainServerImpl triggerQuartz;
    private final SubServerService subServerService;

    public Scanner(MainServerService mainServerService, TriggerQuartzMainServerImpl triggerQuartz, SubServerService subServerService) {
        this.mainServerService = mainServerService;
        this.triggerQuartz = triggerQuartz;
        this.subServerService = subServerService;
        startScan();
    }

    private void startScan() {
        List<MainServerDTO> mainServerDTOList = mainServerService.findAll();
        try {
            triggerQuartz.jobs(mainServerDTOList, mainServerService, subServerService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
