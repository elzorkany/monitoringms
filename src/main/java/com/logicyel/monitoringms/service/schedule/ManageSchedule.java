package com.logicyel.monitoringms.service.schedule;

import com.logicyel.monitoringms.config.Constants;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.service.schedule.mainserver.TriggerQuartzMainServer;
import com.logicyel.monitoringms.service.schedule.stream.TriggerQuartzStream;
import com.logicyel.monitoringms.service.schedule.subserver.TriggerQuartzSubServer;
import com.logicyel.monitoringms.web.rest.MainServerResource;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ManageSchedule {

    private final Logger log = LoggerFactory.getLogger(MainServerResource.class);

    private final TriggerQuartzMainServer triggerQuartzMainServer;
    private final TriggerQuartzSubServer triggerQuartzSubServer;
    private final TriggerQuartzStream triggerQuartzStream;
//    private final MainServerService mainServerService;
//    private final SubServerService subServerService;
//    private final StreamService streamService;

    public ManageSchedule(TriggerQuartzMainServer triggerQuartzMainServer, TriggerQuartzSubServer triggerQuartzSubServer, TriggerQuartzStream triggerQuartzStream) {
        this.triggerQuartzMainServer = triggerQuartzMainServer;
        this.triggerQuartzSubServer = triggerQuartzSubServer;
        this.triggerQuartzStream = triggerQuartzStream;
    }


    public void startMainServerJob(MainServerDTO mainServerDTO, MainServerService mainServerService, SubServerService subServerService) {
        try {
            triggerQuartzMainServer.singleJob(mainServerDTO, mainServerService, subServerService);
        } catch (SchedulerException e) {
            log.error(Constants.STARTJOBERROR, e);
        }
    }


    public void startSubServerJob(SubServerDTO subServerDTO, SubServerService subServerService, StreamService streamService) {
        try {
            triggerQuartzSubServer.singleJob(subServerDTO, subServerService, streamService);
        } catch (SchedulerException e) {
            log.error(Constants.STARTJOBERROR, e);
        }
    }


    public void startStreamJob(StreamDTO streamDTO, StreamService streamService) {
        try {
            triggerQuartzStream.singleJob(streamDTO, streamService);
        } catch (SchedulerException e) {
            log.error(Constants.STARTJOBERROR, e);
        }
    }

    public void restartMainServerJob(MainServerDTO mainServerDTO, MainServerService mainServerService, SubServerService subServerService) {
        try {
            triggerQuartzMainServer.deleteJob(mainServerDTO.getId());
            triggerQuartzMainServer.singleJob(mainServerDTO, mainServerService, subServerService);
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }

    public void restartSubServer(SubServerDTO subServerDTO, SubServerService subServerService, StreamService streamService) {
        try {
            triggerQuartzSubServer.deleteJob(subServerDTO.getId());
            triggerQuartzSubServer.singleJob(subServerDTO, subServerService, streamService);
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }

    public void restartStreamJob(StreamDTO streamDTO, StreamService streamService) {
        try {
            triggerQuartzStream.deleteJob(streamDTO.getId());
            triggerQuartzStream.singleJob(streamDTO, streamService);
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }


    public void deleteMainServerJob(MainServerDTO mainServerDTO) {
        try {
            triggerQuartzMainServer.deleteJob(mainServerDTO.getId());
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }

    public void deleteSubServer(SubServerDTO subServerDTO) {
        try {
            triggerQuartzSubServer.deleteJob(subServerDTO.getId());
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }

    public void deleteStreamJob(StreamDTO streamDTO) {
        try {
            triggerQuartzStream.deleteJob(streamDTO.getId());
        } catch (SchedulerException e) {
            log.error(Constants.DELETEJOBERROR, e);
        }
    }
}
