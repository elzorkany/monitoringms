package com.logicyel.monitoringms.service.schedule.subserver;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.util.Date;
import java.util.Optional;

import static java.lang.System.out;

public class ScanSubServer implements Job {

    @Override
    public void execute(JobExecutionContext arg0) {
        try {

            //"yyyy/MM/dd HH:mm:ss"
            Date date = new Date();
//            ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(),
//                ZoneId.systemDefault());
            DateTime dateTime = new DateTime(date);

            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
            String subServername = dataMap.getString("subServername");
            int subServerIntervalTime = dataMap.getInt("subServerIntervalTime");

            String subServerId = dataMap.getString("subServerId");
            SubServerService subServerService = (SubServerService) dataMap.getOrDefault("subServerService", null);
            StreamService streamService = (StreamService) dataMap.getOrDefault("streamService", null);
            SubServerDTO subServerDTO = (SubServerDTO) dataMap.getOrDefault("subServerDTO", null);

            streamService.scanSubServer(Optional.ofNullable(subServerDTO));


            Optional<SubServerDTO> serverDTO = subServerService.findOne(subServerId);
            serverDTO.ifPresent(subServerDTO1 -> {
                subServerDTO1.setLastScanned(dateTime);
                subServerService.save(serverDTO.get());
            });


            out.println(" subServer :" + subServername + " scanned in " + subServerIntervalTime + " Sec");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
