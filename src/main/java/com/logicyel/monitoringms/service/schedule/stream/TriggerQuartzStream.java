package com.logicyel.monitoringms.service.schedule.stream;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.quartz.SchedulerException;

import java.util.List;

public interface TriggerQuartzStream {
    void jobs(List<StreamDTO> streamDTOS, StreamService streamService) throws SchedulerException;

    void singleJob(StreamDTO streamDTO, StreamService streamService) throws SchedulerException;

    void deleteJob(String streamId) throws SchedulerException;
}
