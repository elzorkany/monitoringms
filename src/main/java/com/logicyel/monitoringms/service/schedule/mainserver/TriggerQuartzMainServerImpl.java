package com.logicyel.monitoringms.service.schedule.mainserver;

import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.System.*;

@Service
public class TriggerQuartzMainServerImpl implements TriggerQuartzMainServer {


    private Scheduler scheduler;

    @Override
    public void jobs(List<MainServerDTO> mainServerDTOList, MainServerService mainServerService, SubServerService subServerService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        for (MainServerDTO mainServerDTO : mainServerDTOList) {
            if (mainServerDTO.getScanInterval() != null) {
                flag = 1;
                commonCode(mainServerDTO, mainServerService, subServerService);
            }
        }
        if (flag == 1)
            scheduler.start();
    }

    @Override
    public void deleteJob(String mainServerId) throws SchedulerException {
        boolean b = scheduler.deleteJob(new JobKey(mainServerId, "group"));
        if (b)
            out.println("mainServer with id " + mainServerId + " is stoped");
    }

    @Override
    public void singleJob(MainServerDTO mainServerDTO, MainServerService mainServerService, SubServerService subServerService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        if (mainServerDTO.getScanInterval() != null) {
            flag = 1;
            commonCode(mainServerDTO, mainServerService, subServerService);
        }
        if (flag == 1)
            scheduler.start();

    }

    private void commonCode(MainServerDTO mainServerDTO, MainServerService mainServerService, SubServerService subServerService) throws SchedulerException {
        JobBuilder jobBuilder = JobBuilder.newJob(ScanMainServer.class);
        JobDetail job = jobBuilder.withIdentity(mainServerDTO.getId(), "group").build();
        int time = mainServerDTO.getScanInterval().intValue();
        if (time > 0) {
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity(mainServerDTO.getId()).withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(time).repeatForever()).build();
            job.getJobDataMap().put("mainServerId", mainServerDTO.getId());
            job.getJobDataMap().put("mainServername", mainServerDTO.getName());
            job.getJobDataMap().put("mainServerIntervalTime", time);
            job.getJobDataMap().put("subServerService", subServerService);
            job.getJobDataMap().put("mainServerService", mainServerService);
            job.getJobDataMap().put("mainServerDTO", mainServerDTO);
            scheduler.scheduleJob(job, trigger);
        }
    }

}
