package com.logicyel.monitoringms.service.schedule.subserver;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.System.out;

@Service
public class TriggerQuartzSubServerImpl implements TriggerQuartzSubServer {


    private Scheduler scheduler;

    @Override
    public void jobs(List<SubServerDTO> subServerDTOS, SubServerService subServerService, StreamService streamService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        for (SubServerDTO subServerDTO : subServerDTOS) {
            if (subServerDTO.getInherentInterval() != null || subServerDTO.getCustomInterval() != null) {
                flag = 1;
                commonCode(subServerDTO, subServerService, streamService);
            }
        }
        if (flag == 1)
            scheduler.start();
    }

    @Override
    public void deleteJob(String subServerId) throws SchedulerException {
        boolean b = scheduler.deleteJob(new JobKey(subServerId, "subServer-group"));
        if (b)
            out.println("subServer with id " + subServerId + " is stoped ");
    }

    @Override
    public void singleJob(SubServerDTO subServerDTO, SubServerService subServerService, StreamService streamService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        if (subServerDTO.getInherentInterval() != null || subServerDTO.getCustomInterval() != null) {
            flag = 1;
            commonCode(subServerDTO, subServerService, streamService);
        }
        if (flag == 1)
            scheduler.start();

    }

    private void commonCode(SubServerDTO subServerDTO, SubServerService subServerService, StreamService streamService) throws SchedulerException {
        JobBuilder jobBuilder = JobBuilder.newJob(ScanSubServer.class);
        JobDetail job = jobBuilder.withIdentity(subServerDTO.getId(), "subServer-group").build();
        int time;
        if (subServerDTO.getCustomInterval() != null) {
            time = subServerDTO.getCustomInterval().intValue();
        } else {
            time = subServerDTO.getInherentInterval().intValue();
        }
        if (time > 0) {
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity(subServerDTO.getId()).withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(time).repeatForever()).build();
            job.getJobDataMap().put("subServerId", subServerDTO.getId());
            job.getJobDataMap().put("subServername", subServerDTO.getName());
            job.getJobDataMap().put("subServerIntervalTime", time);
            job.getJobDataMap().put("subServerService", subServerService);
            job.getJobDataMap().put("subServerDTO", subServerDTO);
            job.getJobDataMap().put("streamService", streamService);
            scheduler.scheduleJob(job, trigger);
        }
    }

}
