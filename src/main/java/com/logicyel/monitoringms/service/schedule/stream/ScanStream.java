package com.logicyel.monitoringms.service.schedule.stream;

import com.logicyel.monitoringms.config.Constants;
import com.logicyel.monitoringms.model.StreamStatus;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.action.EmailTrigger;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.feign.GatewayService;
import com.logicyel.monitoringms.service.mailsender.MailService;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.lang.System.out;

public class ScanStream implements Job {


    @Override
    public void execute(JobExecutionContext arg0) {
        try {

            //"yyyy/MM/dd HH:mm:ss"
            Date date = new Date();
//            ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(),
//                ZoneId.systemDefault());
            DateTime dateTime = new DateTime(date);


            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
            String subServername = dataMap.getString("streamName");
            int subServerIntervalTime = dataMap.getInt("streamIntervalTime");
            StreamDTO streamDTO = (StreamDTO) dataMap.getOrDefault("streamDTO", null);
            String streamId = dataMap.getString("streamId");

            StreamService streamService = (StreamService) dataMap.getOrDefault("streamService", null);
            ScanArchiveService scanArchiveService = (ScanArchiveService) dataMap.getOrDefault("scanArchiveService", null);
            GatewayService gatewayService = (GatewayService) dataMap.getOrDefault("gatewayService", null);
            MailService mailService = (MailService) dataMap.getOrDefault("mailService", null);
            Optional<StreamDTO> streamOne = streamService.findOne(streamId);
            List<StreamStatus> streamStatusList = streamService.getStreamStatus(streamId);

            streamOne.ifPresent(streamDTO1 -> {
                streamDTO1.setLastScanned(dateTime);
                streamDTO1.setStatus(streamStatusList.get(0).getStatus());
                streamService.save(streamOne.get());

                ScanArchiveDTO scanArchiveDTO = new ScanArchiveDTO();
                scanArchiveDTO.setStreamId(streamId);
                scanArchiveDTO.setScanTime(dateTime);
                scanArchiveDTO.setPreviousState(streamOne.get().getStatus());
                scanArchiveDTO.setCurrentState(streamStatusList.get(0).getStatus());
                scanArchiveService.save(scanArchiveDTO);

            });

            String previousState = "";
            if (streamOne.isPresent())
                previousState = streamOne.get().getStatus();
            String currentState = streamStatusList.get(0).getStatus();
            String emailTrigger = streamDTO.getInherentEmailTrigger();
            if (emailTrigger == null)
                emailTrigger = streamDTO.getCustomEmailTrigger();


            getEmailAction(previousState, currentState, emailTrigger, gatewayService, streamDTO, mailService);


            out.println(" stream :" + subServername + " scanned in " + subServerIntervalTime + " Sec and his status is " + streamStatusList.get(0).getStatus());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void getEmailAction(String previousState, String currentState, String emailTrigger, GatewayService gatewayService, StreamDTO streamDTO, MailService mailService) {
        if (previousState.equalsIgnoreCase(Constants.ONLINE) && currentState.equalsIgnoreCase(Constants.OFFLINE) && emailTrigger.equalsIgnoreCase(EmailTrigger.OFFLINE.getValue()))
            out.println("action is offline --- ");

        if (previousState.equalsIgnoreCase(Constants.OFFLINE) && currentState.equalsIgnoreCase(Constants.ONLINE) && emailTrigger.equalsIgnoreCase(EmailTrigger.ONLINE.getValue()))
            out.println("action is online --- ");

        if (previousState.equalsIgnoreCase(currentState) && emailTrigger.equalsIgnoreCase(EmailTrigger.BOTH.getValue())) {
            out.println("action is both --- ");
            List<String> emailByUserId = getEmailByUserId(gatewayService, streamDTO);
            sendMail(emailByUserId, mailService);
        }

    }

    private List<String> getEmailByUserId(GatewayService gatewayService, StreamDTO streamDTO) {
        return gatewayService.getUserEmails(streamDTO.getEmailUserIds());
    }

    private void sendMail(List<String> emails, MailService mailService) {
        for (String email : emails) {
            mailService.sendEmail(email, "test", "testing", false, false);
        }
    }
}
