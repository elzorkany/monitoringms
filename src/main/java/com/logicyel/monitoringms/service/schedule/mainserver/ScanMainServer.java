package com.logicyel.monitoringms.service.schedule.mainserver;

import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

import static java.lang.System.out;

public class ScanMainServer implements Job {

    @Override
    public void execute(JobExecutionContext arg0) {
        try {

            //"yyyy/MM/dd HH:mm:ss"
            Date date = new Date();
//            ZonedDateTime dateTime = ZonedDateTime.ofInstant(date.toInstant(),
//                ZoneId.systemDefault());
            DateTime dateTime = new DateTime(date);



            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
            String mainServername = dataMap.getString("mainServername");
            int mainServerIntervalTime = dataMap.getInt("mainServerIntervalTime");

            String mainServerId = dataMap.getString("mainServerId");
            SubServerService subServerService = (SubServerService) dataMap.getOrDefault("subServerService", null);
            MainServerService mainServerService = (MainServerService) dataMap.getOrDefault("mainServerService", null);

            subServerService.scanSubServers(mainServerId);

            Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(mainServerId);
            mainServerDTO.ifPresent(mainServerDTO1 -> {
                mainServerDTO1.setLastScanned(dateTime);
                mainServerService.save(mainServerDTO.get());

            });


            out.println(" MainServerName :" + mainServername + " scanned each " + mainServerIntervalTime + " Sec");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
