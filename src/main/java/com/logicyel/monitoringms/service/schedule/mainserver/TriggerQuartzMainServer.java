package com.logicyel.monitoringms.service.schedule.mainserver;

import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.quartz.SchedulerException;

import java.util.List;

public interface TriggerQuartzMainServer {
    void jobs(List<MainServerDTO> mainServerDTOList, MainServerService mainServerService, SubServerService subServerService) throws SchedulerException;

    void singleJob(MainServerDTO mainServerDTO,MainServerService mainServerService,SubServerService subServerService)throws SchedulerException;

    void deleteJob(String mainServerId) throws SchedulerException;
}
