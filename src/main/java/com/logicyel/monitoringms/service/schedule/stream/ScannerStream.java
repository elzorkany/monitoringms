package com.logicyel.monitoringms.service.schedule.stream;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScannerStream {

    private final TriggerQuartzStreamImpl triggerQuartzStream;
    private final StreamService streamService;

    public ScannerStream(TriggerQuartzStreamImpl triggerQuartzStream, StreamService streamService) {
        this.triggerQuartzStream = triggerQuartzStream;
        this.streamService = streamService;
        startScan();
    }

    private void startScan() {
        List<StreamDTO> streamDTOList = streamService.findAll();
        try {
            triggerQuartzStream.jobs(streamDTOList, streamService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
