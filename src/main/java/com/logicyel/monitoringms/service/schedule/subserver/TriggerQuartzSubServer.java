package com.logicyel.monitoringms.service.schedule.subserver;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.quartz.SchedulerException;

import java.util.List;

public interface TriggerQuartzSubServer {
    void jobs(List<SubServerDTO> subServerDTOS , SubServerService subServerService, StreamService streamService) throws SchedulerException;

    void singleJob(SubServerDTO subServerDTO, SubServerService subServerService, StreamService streamService)throws SchedulerException;

    void deleteJob(String subServerId) throws SchedulerException;
}
