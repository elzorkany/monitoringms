package com.logicyel.monitoringms.service.schedule.stream;

import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.feign.GatewayService;
import com.logicyel.monitoringms.service.mailsender.MailService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.System.out;

@Service
public class TriggerQuartzStreamImpl implements TriggerQuartzStream {

    @Autowired
    ScanArchiveService scanArchiveService;
    @Autowired
    GatewayService gatewayService;
    @Autowired
    private MailService mailService;

    private Scheduler scheduler;

    @Override
    public void jobs(List<StreamDTO> streamDTOS, StreamService streamService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        for (StreamDTO streamDTO : streamDTOS) {
            if (streamDTO.getInherentInterval() != null || streamDTO.getCustomInterval() != null) {
                flag = 1;
                commonCode(streamDTO, streamService);
            }
        }
        if (flag == 1)
            scheduler.start();
    }

    @Override
    public void deleteJob(String streamId) throws SchedulerException {
        boolean b = scheduler.deleteJob(new JobKey(streamId, "stream-group"));
        if (b)
            out.println("stream with id " + streamId + " is stoped");
    }

    @Override
    public void singleJob(StreamDTO streamDTO, StreamService streamService) throws SchedulerException {
        scheduler = new StdSchedulerFactory().getScheduler();
        int flag = 0;
        if (streamDTO.getInherentInterval() != null || streamDTO.getCustomInterval() != null) {
            flag = 1;
            commonCode(streamDTO, streamService);
        }
        if (flag == 1)
            scheduler.start();

    }

    private void commonCode(StreamDTO streamDTO, StreamService streamService) throws SchedulerException {
        JobBuilder jobBuilder = JobBuilder.newJob(ScanStream.class);
        JobDetail job = jobBuilder.withIdentity(streamDTO.getId(), "stream-group").build();
        int time;
        if (streamDTO.getCustomInterval() != null) {
            time = streamDTO.getCustomInterval().intValue();
        } else {
            time = streamDTO.getInherentInterval().intValue();
        }
        if (time > 0) {
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity(streamDTO.getId()).withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(time).repeatForever()).build();
            job.getJobDataMap().put("streamId", streamDTO.getId());
            job.getJobDataMap().put("streamName", streamDTO.getName());
            job.getJobDataMap().put("streamIntervalTime", time);
            job.getJobDataMap().put("streamDTO", streamDTO);

            job.getJobDataMap().put("streamService", streamService);
            job.getJobDataMap().put("scanArchiveService", scanArchiveService);
            job.getJobDataMap().put("gatewayService", gatewayService);
            job.getJobDataMap().put("mailService", mailService);
            scheduler.scheduleJob(job, trigger);
        }
    }

}
