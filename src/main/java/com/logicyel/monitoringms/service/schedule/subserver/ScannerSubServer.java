package com.logicyel.monitoringms.service.schedule.subserver;

import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScannerSubServer {

    private final TriggerQuartzSubServerImpl triggerQuartz;
    private final SubServerService subServerService;
    private final StreamService streamService;

    public ScannerSubServer(TriggerQuartzSubServerImpl triggerQuartz, SubServerService subServerService, StreamService streamService) {
        this.triggerQuartz = triggerQuartz;
        this.subServerService = subServerService;
        this.streamService = streamService;
        startScan();
    }

    private void startScan() {
        List<SubServerDTO> subServerDTOList = subServerService.findAll();
        try {
            triggerQuartz.jobs(subServerDTOList, subServerService, streamService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
