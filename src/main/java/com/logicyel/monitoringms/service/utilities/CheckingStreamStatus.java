package com.logicyel.monitoringms.service.utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicyel.monitoringms.config.Constants;
import com.logicyel.monitoringms.model.streamjsonmodel.StreamJsonList;
import com.logicyel.monitoringms.model.StreamStatus;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.function.Supplier;

public class CheckingStreamStatus implements Supplier<StreamStatus> {

    private StreamDTO streamDTO;
    private StreamStatus streamStatus = new StreamStatus();
    private final Logger log = LoggerFactory.getLogger(CheckingStreamStatus.class);


    public CheckingStreamStatus(StreamDTO streamDTO) {
        this.streamDTO = streamDTO;
    }

    @Override
    public StreamStatus get() {
        String line;
        Process process;
        StreamJsonList streamJsonList;
        try {
            //            /usr/bin/timeout -t 10   please dd -t parameter befor push to production
            process = Runtime.getRuntime().exec("/usr/bin/timeout -t 10 ffprobe  -v quiet -print_format json -show_streams " + streamDTO.getUrl() + "");
            BufferedReader br = new BufferedReader(
                new InputStreamReader(process.getInputStream()));
            process.waitFor();
            StringBuilder stringBuilder = new
                StringBuilder();
            while ((line = br.readLine()) != null) {

                stringBuilder.append(line);
            }

            try {
                streamJsonList = new ObjectMapper().readValue(stringBuilder.toString(), StreamJsonList.class);
                if (streamJsonList == null || streamJsonList.getStreams() == null || streamJsonList.getStreams().isEmpty()) {
                    streamStatus.setStatus(Constants.OFFLINE);
                } else {
                    streamStatus.setStatus(Constants.ONLINE);
                }
            } catch (Exception ex) {
                streamStatus.setStatus(Constants.OFFLINE);
                log.error(ex.getMessage());
            }

            streamStatus.setId(streamDTO.getId());
            streamStatus.setName(streamDTO.getName());
            streamStatus.setUrl(streamDTO.getUrl());
            streamStatus.setIdOnServer(streamDTO.getIdOnServer());
            streamStatus.setSubServerId(streamDTO.getSubServerId());
            process.destroyForcibly();

        } catch (Exception ex) {
            streamStatus.setStatus(Constants.OFFLINE);
            log.error(ex.getMessage());
        }
        return streamStatus;
    }

    public StreamDTO getStreamDTO() {
        return streamDTO;
    }

    public void setStreamDTO(StreamDTO streamDTO) {
        this.streamDTO = streamDTO;
    }
}
