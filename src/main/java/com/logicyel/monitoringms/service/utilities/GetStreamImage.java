package com.logicyel.monitoringms.service.utilities;

import com.logicyel.monitoringms.model.StreamImage;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.function.Supplier;

public class GetStreamImage implements Supplier<StreamImage> {

    private StreamDTO streamDTO;
    private StreamImage streamImage = new StreamImage();
    private final Logger log = LoggerFactory.getLogger(GetStreamImage.class);


    public GetStreamImage(StreamDTO streamDTO) {
        this.streamDTO = streamDTO;
    }

    @Override
    public StreamImage get() {
        Process process;

        try {
            File tempFile = File.createTempFile("channel", ".jpg");
            String absolutePath = tempFile.getAbsolutePath();
//            /usr/bin/timeout -t 10   please dd -t parameter befor push to production
            process = Runtime.getRuntime().exec("/usr/bin/timeout -t 10 ffmpeg  -i " + streamDTO.getUrl() + " -v quiet -ss 2 -y -t 1 -vframes 1 -vframes 1 -vf " +
                "scale=320:180 -f image2 " + absolutePath);
            process.waitFor();
            streamImage.setId(streamDTO.getId());
            streamImage.setName(streamDTO.getName());
            streamImage.setUrl(streamDTO.getUrl());
            streamImage.setIdOnServer(streamDTO.getIdOnServer());
            streamImage.setSubServerId(streamDTO.getSubServerId());
            byte[] fileContent = Files.readAllBytes(tempFile.toPath());
            streamImage.setImage(fileContent);
            if (!tempFile.delete())
                log.error("Failed to delete the file");
            process.destroyForcibly();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return streamImage;
    }

    public StreamDTO getStreamDTO() {
        return streamDTO;
    }

    public void setStreamDTO(StreamDTO streamDTO) {
        this.streamDTO = streamDTO;
    }

}
