package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.model.SubServerModel;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SubServer.
 */
public interface SubServerService {

    /**
     * Save a subServer.
     *
     * @param subServerDTO the entity to save
     * @return the persisted entity
     */
    SubServerDTO save(SubServerDTO subServerDTO);

    SubServerDTO save(SubServerDTO subServerDTO, SubServerService subServerService);

    SubServerDTO save(MainServerDTO mainServerDTO, MultipartFile file);

    List<SubServerModel> scanSubServers(String mainServer);

    Optional<MainServerDTO> getMainServer(String mainServer);

    List<SubServerDTO> createListSubServer(List<SubServerDTO> subServerDTOS, String mainServerId, SubServerService subServerService);

    List<SubServerDTO> findByMainServer(String transcodingId);

    Page<SubServerDTO> findByMainServerPageable(String transcodingId, Pageable pageable);

    List subServersInSystemButDeleted(List<SubServerModel> subServerModels, String transcodingId);

    Optional<SubServerDTO> setSubserverCustomInterval(String subServerId, Long scanInterval, boolean overrideCustomInterval, SubServerService subServerService);

    List<SubServerDTO> findAll();

    List<SubServerDTO> deleteAllByMainServer(MainServer mainServer);

    void deleteAllStreamsBySubServer(String subServerId);

    Optional<SubServerDTO> setCustomEmailTrigger( String subServerId, String customEmailTrigger,boolean overrideCustomEmailTrigger,List<String> emailUserIds);


    /**
     * Get all the subServer.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubServerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" subServer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SubServerDTO> findOne(String id);

    /**
     * Delete the "id" subServer.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the subServer corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubServerDTO> search(String query, Pageable pageable);
}
