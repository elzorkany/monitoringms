package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.domain.ScanArchive;
import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;

import com.logicyel.monitoringms.service.dto.StreamDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing ScanArchive.
 */
public interface ScanArchiveService {

    /**
     * Save a scanArchive.
     *
     * @param scanArchiveDTO the entity to save
     * @return the persisted entity
     */
    ScanArchiveDTO save(ScanArchiveDTO scanArchiveDTO);
    List<ScanArchive> deleteAllByStreamIn(List<StreamDTO> streamDTOS);
    List<ScanArchive> deleteAllByStream(Stream stream);



    /**
     * Get all the scanArchives.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ScanArchiveDTO> findAll(Pageable pageable);


    /**
     * Get the "id" scanArchive.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ScanArchiveDTO> findOne(String id);

    /**
     * Delete the "id" scanArchive.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the scanArchive corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ScanArchiveDTO> search(String query, Pageable pageable);
}
