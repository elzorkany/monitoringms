package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.model.StreamImage;
import com.logicyel.monitoringms.model.StreamStatus;
import com.logicyel.monitoringms.model.StreamsModel;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Stream.
 */
public interface StreamService {

    Page<StreamImage> streamImage(Pageable pageable) throws IOException, InterruptedException;

    List<Stream> findAllStream();

    List streamInSystemButDeleted(List<StreamsModel> streamsModels, String subId);

    Page<StreamDTO> findBySubServerPageable(String subServerId, Pageable pageable);

    List<StreamDTO> findBySubServer(String subServerId);

    List<StreamsModel> scanSubServer(Optional<SubServerDTO> subServerDTO);

    Optional<MainServerDTO> getMainServer(String mainServer);

    List<StreamDTO> createListStreamServer(List<StreamDTO> streamsOfSubServers, String subServerId, SubServerService subServerService, StreamService streamService);

    Page<StreamStatus> getStreamStatus(Pageable pageable) throws InterruptedException;

    List<StreamStatus> getStreamStatus(String id) throws InterruptedException;

    List<StreamDTO> deleteAllBySubServerIn(List<SubServerDTO> subServerDTOS);

    List<StreamDTO> deleteAllStreamsBySubServer(SubServer subServers);

    void deleteAllScanArchiveByStream(String streamId);

    List<StreamDTO> findAllByInherentIntervalIsNotNullAndSubServerEquals(SubServerDTO subServerDTO);

    List<StreamDTO> findAll();

    Optional<StreamDTO> setStreamCustomInterval(String streamId, Long scanInterval, StreamService streamService);

    Stream toEntity(StreamDTO streamDTO);

    Optional<StreamDTO> setCustomEmailTrigger( String streamId, String customEmailTrigger,List<String> emailUserIds,StreamService streamService);

    /**
     * Save a stream.
     *
     * @param streamDTO the entity to save
     * @return the persisted entity
     */
    StreamDTO save(StreamDTO streamDTO);

    /**
     * Get all the streams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StreamDTO> findAll(Pageable pageable);


    /**
     * Get the "id" stream.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<StreamDTO> findOne(String id);

    /**
     * Delete the "id" stream.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the stream corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StreamDTO> search(String query, Pageable pageable);
}
