package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.service.dto.EmailConfigDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing EmailConfig.
 */
public interface EmailConfigService {


    EmailConfigDTO findLastEmailConfig();

    /**
     * Save a emailConfig.
     *
     * @param emailConfigDTO the entity to save
     * @return the persisted entity
     */
    EmailConfigDTO save(EmailConfigDTO emailConfigDTO);

    /**
     * Get all the emailConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EmailConfigDTO> findAll(Pageable pageable);


    /**
     * Get the "id" emailConfig.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<EmailConfigDTO> findOne(String id);

    /**
     * Delete the "id" emailConfig.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the emailConfig corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EmailConfigDTO> search(String query, Pageable pageable);
}
