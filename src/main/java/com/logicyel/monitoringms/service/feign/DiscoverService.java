package com.logicyel.monitoringms.service.feign;

import com.logicyel.monitoringms.model.StreamsModel;
import com.logicyel.monitoringms.model.SubServerModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
@FeignClient(name = "pluginmanager")
public interface DiscoverService {
    @GetMapping(value = "/api/get-sub-servers")
    List<SubServerModel> getSubServers(@RequestParam("serverType") String serverType, @RequestParam("baseUrl") String baseUrl, @RequestParam("token") String token);

    @GetMapping(value = "/api/get-streams")
    List<StreamsModel> getStreams(@RequestParam("serverType") String serverType, @RequestParam("baseUrl") String baseUrl, @RequestParam("token") String token, @RequestParam("subServerId") int subServerId);

    @PostMapping(value = "/api/get-streams", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    List<StreamsModel> getStreams(@RequestParam("serverType") String serverType, @RequestPart("file") MultipartFile file);
}

