package com.logicyel.monitoringms.service.feign;

import com.codahale.metrics.annotation.Timed;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Component
@FeignClient(name = "monitoring-gateway")
public interface GatewayService {
//    @GetMapping(value = "/api/get-sub-servers")
//    List<SubServerModel> getSubServers(@RequestParam("serverType") String serverType, @RequestParam("baseUrl") String baseUrl, @RequestParam("token") String token);


    @PostMapping(value = "/api/users/get-users-emails")
    @Timed
    List<String> getUserEmails(@RequestBody List<String> userId);
}
