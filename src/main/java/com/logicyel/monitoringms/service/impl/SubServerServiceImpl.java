package com.logicyel.monitoringms.service.impl;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.model.SubServerModel;
import com.logicyel.monitoringms.repository.SubServerRepository;
import com.logicyel.monitoringms.repository.search.SubServerSearchRepository;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.UploadService;
import com.logicyel.monitoringms.service.action.EmailTrigger;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.service.feign.DiscoverService;
import com.logicyel.monitoringms.service.mapper.MainServerMapperImpl;
import com.logicyel.monitoringms.service.mapper.SubServerMapper;
import com.logicyel.monitoringms.service.schedule.ManageSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.logicyel.monitoringms.model.SubServerCategory.convertOriginModelToDomainDTO;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing SubServer.
 */
@Service
public class SubServerServiceImpl implements SubServerService {

    private final Logger log = LoggerFactory.getLogger(SubServerServiceImpl.class);

    private final SubServerRepository subServerRepository;

    private final SubServerMapper subServerMapper;
    private final SubServerSearchRepository subServerSearchRepository;
    private final MainServerServiceImpl mainServerService;
    private final MainServerMapperImpl mainServerMapper;
    private final DiscoverService discoverService;
    private final UploadService uploadService;
    private final StreamService streamService;
    private final ScanArchiveService scanArchiveService;
    private final ManageSchedule manageSchedule;

    public SubServerServiceImpl(SubServerRepository subServerRepository, SubServerMapper subServerMapper,
                                SubServerSearchRepository subServerSearchRepository,
                                MainServerServiceImpl mainServerService,
                                MainServerMapperImpl mainServerMapper, DiscoverService discoverService, UploadService uploadService, StreamService streamService, ScanArchiveService scanArchiveService, ManageSchedule manageSchedule) {
        this.subServerRepository = subServerRepository;
        this.subServerMapper = subServerMapper;
        this.subServerSearchRepository = subServerSearchRepository;
        this.mainServerService = mainServerService;
        this.mainServerMapper = mainServerMapper;
        this.discoverService = discoverService;
        this.uploadService = uploadService;
        this.streamService = streamService;
        this.scanArchiveService = scanArchiveService;
        this.manageSchedule = manageSchedule;
    }

    /**
     * Save a subServer.
     *
     * @param subServerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubServerDTO save(SubServerDTO subServerDTO) {
        log.debug("Request to save SubServer : {}", subServerDTO);
        SubServer subServer = subServerMapper.toEntity(subServerDTO);
        subServer = subServerRepository.save(subServer);
        SubServerDTO result = subServerMapper.toDto(subServer);
        subServerSearchRepository.save(subServer);
        return result;
    }

    @Override
    public SubServerDTO save(SubServerDTO subServerDTO, SubServerService subServerService) {

        SubServerDTO savedSubServerDto = save(subServerDTO);
        manageSchedule.startSubServerJob(savedSubServerDto, subServerService, streamService);
        return savedSubServerDto;
    }

    @Override
    public SubServerDTO save(MainServerDTO mainServerDTO, MultipartFile file) {
        File uploadedFile = uploadService.uploadFile(file);
        SubServerDTO subServerDTO = new SubServerDTO();
        subServerDTO.setBaseUrl(Paths.get(uploadedFile.getAbsolutePath()).toString());
        subServerDTO.setIdOnServer(-1);
        subServerDTO.setName(file.getOriginalFilename());
        subServerDTO.setMainServerId(mainServerDTO.getId());
        save(subServerDTO);

        return subServerDTO;
    }

    @Override
    public List<SubServerModel> scanSubServers(String mainServerid) {
        Optional<MainServerDTO> mainServerDTO = getMainServer(mainServerid);
        if (!mainServerDTO.isPresent())
            throw new ExceptionInInitializerError("missing id of mainServer : {}" + mainServerid);
        MainServer mainServer = mainServerMapper.toEntity(mainServerDTO.get());
        try {
            boolean url = ResourceUtils.isUrl(mainServer.getBaseUrl());
            if (url) {
                String encode = URLEncoder.encode(mainServer.getBaseUrl(), "UTF-8");
                return discoverService.getSubServers(mainServer.getServerType(), encode, mainServer.getToken());
            } else {
                manageSchedule.deleteMainServerJob(mainServerDTO.get());
                throw new ExceptionInInitializerError("this main server has a file : {}" + mainServerid);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }


    @Override
    public Optional<MainServerDTO> getMainServer(String mainServer) {
        Optional<MainServerDTO> mainServerDTO;
        mainServerDTO = mainServerService.findOne(mainServer);
        return mainServerDTO;
    }

    /**
     * Get all the SubServers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<SubServerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubServers");
        return subServerRepository.findAll(pageable)
            .map(subServerMapper::toDto);
    }


    /**
     * Get one subServer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<SubServerDTO> findOne(String id) {
        log.debug("Request to get SubServer : {}", id);
        return subServerRepository.findById(id)
            .map(subServerMapper::toDto);
    }

    /**
     * Delete the subServer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete SubServer : {}", id);
        subServerRepository.deleteById(id);
        subServerSearchRepository.deleteById(id);
    }

    @Override
    public List<SubServerDTO> deleteAllByMainServer(MainServer mainServer) {
        List<SubServer> subServers = subServerRepository.deleteAllByMainServer(mainServer);
        for (SubServer subServer : subServers) {
            subServerSearchRepository.deleteById(subServer.getId());
        }
        return subServerMapper.toDto(subServers);
    }

    @Override
    public void deleteAllStreamsBySubServer(String subServerId) {
        Optional<SubServerDTO> subServerDTO = findOne(subServerId);
        subServerDTO.ifPresent(subServerDTO1 -> {
            SubServer subServer = subServerMapper.toEntity(subServerDTO1);
            List<StreamDTO> streamDTOS = streamService.deleteAllStreamsBySubServer(subServer);
            for (StreamDTO streamDTO : streamDTOS) {
                manageSchedule.deleteStreamJob(streamDTO);
            }
            scanArchiveService.deleteAllByStreamIn(streamDTOS);
            manageSchedule.deleteSubServer(subServerDTO1);
            delete(subServerDTO1.getId());

        });
    }

    @Override
    public Optional<SubServerDTO> setCustomEmailTrigger(String subServerId, String customEmailTrigger, boolean overrideCustomEmailTrigger, List<String> emailUserIds) {
        Optional<SubServerDTO> subServerDTO = findOne(subServerId);
        subServerDTO.ifPresent(subServerDTO1 -> {
//            String emailTrigger = subServerDTO1.getCustomEmailTrigger();
//            if (emailTrigger.isEmpty())
//                emailTrigger = subServerDTO1.getInherentEmailTrigger();

//            List<String> userIds = subServerDTO1.getEmailUserIds();
//            if (userIds.isEmpty() && !emailUserIds.isEmpty()) {
//
//            }
//            if (!emailTrigger.equalsIgnoreCase(customEmailTrigger)) {

                for (EmailTrigger trigger : EmailTrigger.values()) {
                    if (trigger.getValue().equalsIgnoreCase(customEmailTrigger)) {
                        subServerDTO1.setCustomEmailTrigger(trigger.name());
                    }
                }
                if (subServerDTO1.getCustomEmailTrigger() != null) {
                    if (emailUserIds != null)
                        subServerDTO1.setEmailUserIds(emailUserIds);
                    subServerDTO1.setInherentEmailTrigger(null);
                }
                if (EmailTrigger.NO_ACTION.getValue().equalsIgnoreCase(subServerDTO1.getCustomEmailTrigger()))
                    subServerDTO1.setEmailUserIds(null);

                SubServerDTO savedSubServerDTO = save(subServerDTO1);

                overrideStreamEmailTrigger(savedSubServerDTO, overrideCustomEmailTrigger);
//            }
        });
        return subServerDTO;
    }

    private void overrideStreamEmailTrigger(SubServerDTO savedSubServerDTO, boolean overrideCustomEmailTrigger) {
        if (savedSubServerDTO != null) {
            List<StreamDTO> streamDTOList = streamService.findBySubServer(savedSubServerDTO.getId());
            for (StreamDTO streamDTO : streamDTOList) {
                if (overrideCustomEmailTrigger) {
                    streamDTO.setInherentEmailTrigger(savedSubServerDTO.getCustomEmailTrigger());
                    streamDTO.setEmailUserIds(savedSubServerDTO.getEmailUserIds());
                    streamDTO.setCustomEmailTrigger(null);
                } else {
                    if (streamDTO.getCustomEmailTrigger() == null) {
                        streamDTO.setInherentEmailTrigger(savedSubServerDTO.getCustomEmailTrigger());
                        streamDTO.setEmailUserIds(savedSubServerDTO.getEmailUserIds());
                    }
                }
                StreamDTO savedStream = streamService.save(streamDTO);
                if (savedStream != null)
                    manageSchedule.restartStreamJob(savedStream, streamService);
            }
        }
    }

    /**
     * Search for the subServer corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<SubServerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SubServers for query {}", query);
        return subServerSearchRepository.search(queryStringQuery(query), pageable).map(subServerMapper::toDto);
    }

    @Override
    public List<SubServerDTO> createListSubServer(List<SubServerDTO> subServerDTOS,
                                                  String mainServerId, SubServerService subServerService) {
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(mainServerId);
        List<SubServerDTO> result = new ArrayList<>();
        for (SubServerDTO subServerDTO : subServerDTOS) {
            try {
                if (subServerDTO.getCustomInterval() == null) {
                    mainServerDTO.ifPresent(mainServerDTO1 -> subServerDTO.setInherentInterval(mainServerDTO1.getScanInterval()));
                }
                if (subServerDTO.getCustomEmailTrigger() == null) {
                    mainServerDTO.ifPresent(mainServerDTO1 -> setInherentEmailTrigger(mainServerDTO1, subServerDTO));
                } else {
                    mainServerDTO.ifPresent(mainServerDTO1 -> {
                        if (subServerDTO.getEmailUserIds() == null)
                            subServerDTO.setEmailUserIds(mainServerDTO1.getEmailUserIds());
                    });
                }
                subServerDTO.setMainServerId(mainServerId);
                SubServerDTO savedSubServerDto = save(subServerDTO);
                result.add(savedSubServerDto);
                if (savedSubServerDto != null)
                    manageSchedule.startSubServerJob(savedSubServerDto, subServerService, streamService);
            } catch (Exception e) {
                log.debug("this model have duplicated base url ", e);
            }
        }
        return result;
    }

    private void setInherentEmailTrigger(MainServerDTO mainServerDTO, SubServerDTO subServerDTO) {
        subServerDTO.setInherentEmailTrigger(mainServerDTO.getEmailTrigger());
        if (subServerDTO.getEmailUserIds() == null)
            subServerDTO.setEmailUserIds(mainServerDTO.getEmailUserIds());
        subServerDTO.setMainServerId(mainServerDTO.getId());
    }

    @Override
    public List<SubServerDTO> findByMainServer(String transcodingId) {
        return subServerMapper.toDto(subServerRepository.findByMainServer(transcodingId));
    }

    @Override
    public Page<SubServerDTO> findByMainServerPageable(String transcodingId, Pageable pageable) {
        return subServerRepository.findByMainServer(transcodingId, pageable).map(subServerMapper::toDto);
    }


    @Override
    public List subServersInSystemButDeleted(List<SubServerModel> subServerModels, String transcodingId) {
        List<SubServerDTO> originServerList = convertOriginModelToDomainDTO(subServerModels);

        List<SubServerDTO> subServerDTO = findByMainServer(transcodingId);
        subServerDTO.removeAll(originServerList);

        return subServerDTO;

    }

    @Override
    public Optional<SubServerDTO> setSubserverCustomInterval(String subServerId, Long scanInterval, boolean overrideCustomInterval, SubServerService subServerService) {
        Optional<SubServerDTO> subServerDTO = findOne(subServerId);
        subServerDTO.ifPresent(subServerDTO1 -> {
            if (!subServerDTO1.getCustomInterval().equals(scanInterval)) {
                subServerDTO1.setCustomInterval(scanInterval);
                if (subServerDTO1.getCustomInterval() != null) {
                    subServerDTO1.setInherentInterval(null);
                }
                SubServerDTO savedSubServer = save(subServerDTO1);
                if (savedSubServer != null) {
                    if (overrideCustomInterval) {
                        customIntervalTrue(savedSubServer, scanInterval);
                    } else {
                        customIntervalFalse(savedSubServer, scanInterval);
                    }
                }
                manageSchedule.restartSubServer(subServerDTO1, subServerService, streamService);
            }
        });
        return subServerDTO;
    }

    private void customIntervalTrue(SubServerDTO subServerDTO, Long scanInterval) {
        List<StreamDTO> streamDTOList = streamService.findBySubServer(subServerDTO.getId());
        for (StreamDTO streamDTO : streamDTOList) {
            streamDTO.setInherentInterval(scanInterval);
            streamDTO.setCustomInterval(null);
            StreamDTO savedStreamDTO = streamService.save(streamDTO);
            if (savedStreamDTO != null)
                manageSchedule.restartStreamJob(savedStreamDTO, streamService);
        }
    }

    private void customIntervalFalse(SubServerDTO subServerDTO, Long scanInterval) {
        List<StreamDTO> streamDTOList = streamService.findAllByInherentIntervalIsNotNullAndSubServerEquals(subServerDTO);
        for (StreamDTO streamDTO : streamDTOList) {
            streamDTO.setInherentInterval(scanInterval);
            StreamDTO savedStreamDTO = streamService.save(streamDTO);
            if (savedStreamDTO != null)
                manageSchedule.restartStreamJob(savedStreamDTO, streamService);
        }
    }

    @Override
    public List<SubServerDTO> findAll() {
        List<SubServer> subServers = subServerRepository.findAllByIdNotNull();
        return subServerMapper.toDto(subServers);
    }
}
