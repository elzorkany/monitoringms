package com.logicyel.monitoringms.service.impl;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.domain.SubServer;
import com.logicyel.monitoringms.model.StreamImage;
import com.logicyel.monitoringms.model.StreamStatus;
import com.logicyel.monitoringms.model.StreamsModel;
import com.logicyel.monitoringms.repository.StreamRepository;
import com.logicyel.monitoringms.repository.search.StreamSearchRepository;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.action.EmailTrigger;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.service.feign.DiscoverService;
import com.logicyel.monitoringms.service.mapper.MainServerMapper;
import com.logicyel.monitoringms.service.mapper.StreamMapper;
import com.logicyel.monitoringms.service.mapper.SubServerMapper;
import com.logicyel.monitoringms.service.schedule.ManageSchedule;
import com.logicyel.monitoringms.service.utilities.CheckingStreamStatus;
import com.logicyel.monitoringms.service.utilities.GetStreamImage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.logicyel.monitoringms.model.StreamCategory.convertStreamModelToDomainDTO;
import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Stream.
 */
@Service
public class StreamServiceImpl implements StreamService {

    private final Logger log = LoggerFactory.getLogger(StreamServiceImpl.class);

    private final StreamRepository streamRepository;
    private final StreamMapper streamMapper;
    private final StreamSearchRepository streamSearchRepository;

    private final DiscoverService discoverService;
    private final MainServerService mainServerService;
    private final MainServerMapper mainServerMapper;
    private final SubServerMapper subServerMapper;
    private final ScanArchiveService scanArchiveService;
    private final ManageSchedule manageSchedule;

    public StreamServiceImpl(StreamRepository streamRepository, StreamMapper streamMapper, StreamSearchRepository streamSearchRepository, DiscoverService discoverService, MainServerService mainServerService, MainServerMapper mainServerMapper, SubServerMapper subServerMapper, ScanArchiveService scanArchiveService, ManageSchedule manageSchedule) {
        this.streamRepository = streamRepository;
        this.streamMapper = streamMapper;
        this.streamSearchRepository = streamSearchRepository;

        this.discoverService = discoverService;
        this.mainServerService = mainServerService;
        this.mainServerMapper = mainServerMapper;
        this.subServerMapper = subServerMapper;
        this.scanArchiveService = scanArchiveService;
        this.manageSchedule = manageSchedule;
    }


    @Override
    public List<Stream> findAllStream() {
        return streamRepository.findAll();
    }


    @Override
    public List streamInSystemButDeleted(List<StreamsModel> streamsModels, String subId) {
        List<StreamDTO> streamList = convertStreamModelToDomainDTO(streamsModels);
        List<StreamDTO> streams = findBySubServer(subId);
        streams.removeAll(streamList);
        return streams;

    }

    @Override
    public Page<StreamDTO> findBySubServerPageable(String subServerId, Pageable pageable) {
        return streamRepository.findBySubServer(subServerId, pageable).map(streamMapper::toDto);
    }

    @Override
    public List<StreamDTO> findBySubServer(String subServerId) {
        return streamMapper.toDto(streamRepository.findBySubServer(subServerId));
    }

    /**
     * Save a stream.
     *
     * @param streamDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public StreamDTO save(StreamDTO streamDTO) {
        log.debug("Request to save Stream : {}", streamDTO);
        Stream stream = streamMapper.toEntity(streamDTO);
        stream = streamRepository.save(stream);
        StreamDTO result = streamMapper.toDto(stream);
        streamSearchRepository.save(stream);
        return result;
    }

    /**
     * Get all the streams.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<StreamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Streams");
        return streamRepository.findAll(pageable)
            .map(streamMapper::toDto);
    }


    /**
     * Get one stream by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<StreamDTO> findOne(String id) {
        log.debug("Request to get Stream : {}", id);
        return streamRepository.findById(id)
            .map(streamMapper::toDto);
    }

    /**
     * Delete the stream by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Stream : {}", id);
        streamRepository.deleteById(id);
        streamSearchRepository.deleteById(id);
    }

    @Override
    public List<StreamDTO> deleteAllBySubServerIn(List<SubServerDTO> subServerDTOS) {
        List<Stream> streams = streamRepository.deleteAllBySubServerIn(subServerMapper.toEntity(subServerDTOS));
        for (Stream stream : streams) {
            streamSearchRepository.deleteById(stream.getId());
        }
        return streamMapper.toDto(streams);
    }

    @Override
    public List<StreamDTO> deleteAllStreamsBySubServer(SubServer subServers) {
        List<Stream> streams = streamRepository.deleteAllBySubServer(subServers);
        for (Stream stream : streams) {
            streamSearchRepository.deleteById(stream.getId());
        }
        return streamMapper.toDto(streams);
    }

    @Override
    public void deleteAllScanArchiveByStream(String streamId) {
        Optional<StreamDTO> streamDTO = findOne(streamId);
        streamDTO.ifPresent(streamDTO1 -> {
            Stream stream = streamMapper.toEntity(streamDTO1);
            manageSchedule.deleteStreamJob(streamDTO1);
            scanArchiveService.deleteAllByStream(stream);
            delete(streamId);
        });
    }


    /**
     * Search for the stream corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<StreamDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Streams for query {}", query);
        Page<Stream> search = streamSearchRepository.search(queryStringQuery(query), pageable);
        return search.map(streamMapper::toDto);
    }


    @Override
    public List<StreamDTO> createListStreamServer(List<StreamDTO> streamDTOs, String subServerId, SubServerService subServerService, StreamService streamService) {
        List<StreamDTO> result = new ArrayList<>();
        Optional<SubServerDTO> subServerDTO = subServerService.findOne(subServerId);
        if (!subServerDTO.isPresent())
            log.debug("subServer Id not found {} ", subServerId);
        for (StreamDTO streamDTO : streamDTOs) {
            try {
                streamDTO.setSubServerId(subServerId);
                if (streamDTO.getCustomInterval() == null) {
                    subServerDTO.ifPresent(subServerDTO1 -> {
                        if (subServerDTO1.getInherentInterval() != null) {
                            streamDTO.setInherentInterval(subServerDTO1.getInherentInterval());
                        } else {
                            streamDTO.setInherentInterval(subServerDTO1.getCustomInterval());
                        }
                    });
                }
                subServerDTO.ifPresent(subServerDTO1 -> setStreamEmailTrigger(streamDTO, subServerDTO1));
                StreamDTO savedStreamDTO = save(streamDTO);
                if (savedStreamDTO != null)
                    manageSchedule.startStreamJob(savedStreamDTO, streamService);
                result.add(savedStreamDTO);
            } catch (Exception e) {
                log.debug("this model have duplicated base url ");
            }
        }
        return result;
    }

    private void setStreamEmailTrigger(StreamDTO streamDTO, SubServerDTO subServerDTO) {
        if (streamDTO.getCustomEmailTrigger() == null) {
            if (subServerDTO.getInherentEmailTrigger() != null) {
                streamDTO.setInherentEmailTrigger(subServerDTO.getInherentEmailTrigger());
                streamDTO.setEmailUserIds(subServerDTO.getEmailUserIds());
            } else {
                streamDTO.setInherentEmailTrigger(subServerDTO.getCustomEmailTrigger());
                streamDTO.setEmailUserIds(subServerDTO.getEmailUserIds());
            }
        } else {
            if (streamDTO.getEmailUserIds() == null)
                streamDTO.setEmailUserIds(subServerDTO.getEmailUserIds());
        }
    }

    @Override
    public Optional<StreamDTO> setStreamCustomInterval(String streamId, Long scanInterval, StreamService
        streamService) {
        Optional<StreamDTO> streamDTO = findOne(streamId);
        streamDTO.ifPresent(streamDTO1 -> {
//            if (streamDTO1.getCustomInterval() != scanInterval) {
                streamDTO1.setCustomInterval(scanInterval);
                if (streamDTO1.getCustomInterval() != null) {
                    streamDTO1.setInherentInterval(null);
                }
                StreamDTO savedStream = save(streamDTO1);
                if (savedStream != null)
                    manageSchedule.restartStreamJob(savedStream, streamService);
//            }
        });
        return streamDTO;
    }

    @Override
    public Stream toEntity(StreamDTO streamDTO) {
        return streamMapper.toEntity(streamDTO);
    }

    @Override
    public Optional<StreamDTO> setCustomEmailTrigger(String streamId, String customEmailTrigger, List<String> emailUserIds, StreamService streamService) {
        Optional<StreamDTO> streamDTO = findOne(streamId);
        streamDTO.ifPresent(streamDTO1 -> {
//            if (!streamDTO1.getCustomEmailTrigger().equalsIgnoreCase(customEmailTrigger) || !streamDTO1.getEmailUserIds().equals(emailUserIds)) {
                for (EmailTrigger trigger : EmailTrigger.values()) {
                    if (trigger.getValue().equalsIgnoreCase(customEmailTrigger)) {
                        streamDTO1.setCustomEmailTrigger(trigger.name());
                    }
                }
                if (streamDTO1.getCustomEmailTrigger() != null) {
                    if (emailUserIds != null)
                        streamDTO1.setEmailUserIds(emailUserIds);
                    streamDTO1.setInherentEmailTrigger(null);
                }
                if (EmailTrigger.NO_ACTION.getValue().equalsIgnoreCase(streamDTO1.getCustomEmailTrigger()))
                    streamDTO1.setEmailUserIds(null);

                StreamDTO savedStream = save(streamDTO1);
                if (savedStream != null)
                    manageSchedule.restartStreamJob(savedStream, streamService);
//            }
        });
        return streamDTO;
    }

    @Override
    public Optional<MainServerDTO> getMainServer(String mainServer) {
        Optional<MainServerDTO> mainServerDTO;
        mainServerDTO = mainServerService.findOne(mainServer);
        return mainServerDTO;
    }

    @Override
    public List<StreamsModel> scanSubServer(Optional<SubServerDTO> subServerDTO) {
        if (!subServerDTO.isPresent())
            throw new NullPointerException();
        int idOnServer = subServerDTO.get().getIdOnServer();
        String mainServerId = subServerDTO.get().getMainServerId();
        Optional<MainServerDTO> mainServerDTO = getMainServer(mainServerId);

        if (!mainServerDTO.isPresent())
            throw new NullPointerException();

        MainServer mainServer = mainServerMapper.toEntity(mainServerDTO.get());
        try {
            if (idOnServer != -1) {
                String encode = URLEncoder.encode(mainServer.getBaseUrl(), "UTF-8");
                return discoverService.getStreams(mainServer.getServerType(), encode, mainServer.getToken(), idOnServer);
            } else {
                File file = new File(subServerDTO.get().getBaseUrl());
                try {
                    FileInputStream input = new FileInputStream(file);
                    MultipartFile multipartFile = new MockMultipartFile("file",
                        file.getName(), "text/plain", IOUtils.toByteArray(input));
                    return discoverService.getStreams(mainServer.getServerType(), multipartFile);
                } catch (Exception e) {
                    log.error("can't discover the file");
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * Get all the getStreamStatus.
     *
     * @return the List of entities
     */

    @Override
    public Page<StreamStatus> getStreamStatus(Pageable pageable) throws InterruptedException {
        Page<StreamDTO> streamDTOS = findAll(pageable);
        Executor executor = Executors.newCachedThreadPool();
        CompletableFuture[] completableFutures = new CompletableFuture[streamDTOS.getNumberOfElements()];
        int i = 0;
        for (StreamDTO streamdto : streamDTOS) {
            CheckingStreamStatus checkingStreamStatus = new CheckingStreamStatus(streamdto);
            CompletableFuture<StreamStatus> streamStatusCompletableFuture = CompletableFuture.supplyAsync(checkingStreamStatus, executor);
            completableFutures[i] = streamStatusCompletableFuture;
            i++;
        }
        try {
            return new PageImpl<>(applyCompletableFuturesForStatus2(completableFutures).get(), pageable, streamDTOS.getTotalElements());
        } catch (ExecutionException e) {
            log.error("getStreamStatus has no value", e);
        }
        return Page.empty();
    }

    @Override
    public List<StreamStatus> getStreamStatus(String streamId) throws InterruptedException {
        Optional<StreamDTO> streamDTO = findOne(streamId);
        Executor executor = Executors.newCachedThreadPool();
        CompletableFuture completableFutures;
        if (streamDTO.isPresent()) {
            CheckingStreamStatus checkingStreamStatus = new CheckingStreamStatus(streamDTO.get());
            completableFutures = CompletableFuture.supplyAsync(checkingStreamStatus, executor);

            try {
                return applyCompletableFuturesForStatus(completableFutures).get();
            } catch (ExecutionException e) {
                log.error("getStreamStatus has no value", e);
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Page<StreamImage> streamImage(Pageable pageable) throws InterruptedException {
        Page<StreamDTO> streamDTOS = findAll(pageable);
        Executor executor = Executors.newCachedThreadPool();
        CompletableFuture[] completableFutures = new CompletableFuture[streamDTOS.getNumberOfElements()];
        int i = 0;
        for (StreamDTO streamdto : streamDTOS) {
            GetStreamImage streamImageCompletableFuture = new GetStreamImage(streamdto);
            CompletableFuture<StreamImage> imageCompletableFuture = CompletableFuture.supplyAsync(streamImageCompletableFuture, executor);
            completableFutures[i] = imageCompletableFuture;
            i++;
        }
        try {
            return new PageImpl<>(applyCompletableFuturesForImage(completableFutures).get(), pageable, streamDTOS.getTotalElements());
        } catch (ExecutionException e) {
            log.error("streamImage has no value", e);
        }
        return Page.empty();

    }

    private CompletableFuture<List<StreamStatus>> applyCompletableFuturesForStatus(CompletableFuture<?>...
                                                                                       futures) {
        return CompletableFuture.allOf(futures)
            .thenApply(x -> Arrays.stream(futures)
                .map(f -> (StreamStatus) f.join())
                .collect(toList()));
    }

    private CompletableFuture<List<StreamStatus>> applyCompletableFuturesForStatus2(CompletableFuture<?>...
                                                                                        futures) {
        return CompletableFuture.allOf(futures)
            .thenApply(x -> Arrays.stream(futures)
                .map(f -> (StreamStatus) f.join())
                .collect(toList()));
    }

    private CompletableFuture<List<StreamImage>> applyCompletableFuturesForImage(CompletableFuture<?>... futures) {
        return CompletableFuture.allOf(futures)
            .thenApply(x -> Arrays.stream(futures)
                .map(f -> (StreamImage) f.join())
                .collect(toList()));
    }

    @Override
    public List<StreamDTO> findAll() {
        List<Stream> streamList = streamRepository.findAll();
        return streamMapper.toDto(streamList);
    }


    @Override
    public List<StreamDTO> findAllByInherentIntervalIsNotNullAndSubServerEquals(SubServerDTO subServerDTO) {
        SubServer subServer = subServerMapper.toEntity(subServerDTO);
        List<Stream> streams = streamRepository.findAllByCustomIntervalIsNullAndSubServerEquals(subServer);
        return streamMapper.toDto(streams);
    }

}
