package com.logicyel.monitoringms.service.impl;

import com.logicyel.monitoringms.service.EmailConfigService;
import com.logicyel.monitoringms.domain.EmailConfig;
import com.logicyel.monitoringms.repository.EmailConfigRepository;
import com.logicyel.monitoringms.repository.search.EmailConfigSearchRepository;
import com.logicyel.monitoringms.service.dto.EmailConfigDTO;
import com.logicyel.monitoringms.service.mapper.EmailConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing EmailConfig.
 */
@Service
public class EmailConfigServiceImpl implements EmailConfigService {

    private final Logger log = LoggerFactory.getLogger(EmailConfigServiceImpl.class);

    private final EmailConfigRepository emailConfigRepository;

    private final EmailConfigMapper emailConfigMapper;

    private final EmailConfigSearchRepository emailConfigSearchRepository;

    public EmailConfigServiceImpl(EmailConfigRepository emailConfigRepository, EmailConfigMapper emailConfigMapper, EmailConfigSearchRepository emailConfigSearchRepository) {
        this.emailConfigRepository = emailConfigRepository;
        this.emailConfigMapper = emailConfigMapper;
        this.emailConfigSearchRepository = emailConfigSearchRepository;
    }

    @Override
    public EmailConfigDTO findLastEmailConfig() {
        return emailConfigMapper.toDto(emailConfigRepository.findTopByIdNotNullOrderByIdDesc());
    }

    /**
     * Save a emailConfig.
     *
     * @param emailConfigDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EmailConfigDTO save(EmailConfigDTO emailConfigDTO) {
        log.debug("Request to save EmailConfig : {}", emailConfigDTO);
        EmailConfig emailConfig = emailConfigMapper.toEntity(emailConfigDTO);
        emailConfig = emailConfigRepository.save(emailConfig);
        EmailConfigDTO result = emailConfigMapper.toDto(emailConfig);
        emailConfigSearchRepository.save(emailConfig);
        return result;
    }

    /**
     * Get all the emailConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<EmailConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EmailConfigs");
        return emailConfigRepository.findAll(pageable)
            .map(emailConfigMapper::toDto);
    }


    /**
     * Get one emailConfig by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<EmailConfigDTO> findOne(String id) {
        log.debug("Request to get EmailConfig : {}", id);
        return emailConfigRepository.findById(id)
            .map(emailConfigMapper::toDto);
    }

    /**
     * Delete the emailConfig by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete EmailConfig : {}", id);
        emailConfigRepository.deleteById(id);
        emailConfigSearchRepository.deleteById(id);
    }

    /**
     * Search for the emailConfig corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<EmailConfigDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EmailConfigs for query {}", query);
        return emailConfigSearchRepository.search(queryStringQuery(query), pageable)
            .map(emailConfigMapper::toDto);
    }
}
