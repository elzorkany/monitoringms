package com.logicyel.monitoringms.service.impl;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.repository.MainServerRepository;
import com.logicyel.monitoringms.repository.search.MainServerSearchRepository;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.action.EmailTrigger;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.service.mapper.MainServerMapper;
import com.logicyel.monitoringms.service.schedule.ManageSchedule;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing mainServer.
 */
@Service
public class MainServerServiceImpl implements MainServerService {

    private final Logger log = LoggerFactory.getLogger(MainServerServiceImpl.class);

    private final MainServerRepository mainServerRepository;
    private final MainServerMapper mainServerMapper;
    private final MainServerSearchRepository mainServerSearchRepository;
    private final ScanArchiveService scanArchiveService;
    private final ManageSchedule manageSchedule;

    public MainServerServiceImpl(MainServerRepository mainServerRepository, MainServerMapper mainServerMapper, MainServerSearchRepository mainServerSearchRepository, ScanArchiveService scanArchiveService, ManageSchedule manageSchedule) {
        this.mainServerRepository = mainServerRepository;
        this.mainServerMapper = mainServerMapper;
        this.mainServerSearchRepository = mainServerSearchRepository;
        this.scanArchiveService = scanArchiveService;
        this.manageSchedule = manageSchedule;

    }


    /**
     * Save a mainServer.
     *
     * @param mainServerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MainServerDTO save(MainServerDTO mainServerDTO) {
        log.debug("Request to save mainServer : {}", mainServerDTO);
        MainServer mainServer = mainServerMapper.toEntity(mainServerDTO);
        mainServer = mainServerRepository.save(mainServer);
        MainServerDTO result = mainServerMapper.toDto(mainServer);
        mainServerSearchRepository.save(mainServer);

        return result;
    }

    @Override
    public MainServerDTO save(MainServerDTO mainServerDTO, MainServerService mainServerService, SubServerService subServerService) {
        if (mainServerDTO.getScanInterval() == null)
            mainServerDTO.setScanInterval(0L);
        MainServerDTO savedMainServerDto = new MainServerDTO();
        if (mainServerDTO.getScanInterval() >= 60L) {
            savedMainServerDto = save(mainServerDTO);
            manageSchedule.startMainServerJob(savedMainServerDto, mainServerService, subServerService);
        }
        return savedMainServerDto;
    }

    @Override
    public MainServerDTO save(String serverType, String mainServerName, MultipartFile file) {
        try {
            MainServerDTO mainServerDTO = new MainServerDTO();
            mainServerDTO.setBaseUrl(file.getOriginalFilename());
            mainServerDTO.setName(mainServerName);
            mainServerDTO.setServerType(serverType);

            return save(mainServerDTO);
        } catch (Exception e) {
            log.error("file is already inserted , you can't duplicate the file ");
            throw new BadRequestAlertException("A new mainServer cannot already have this file", "", "file exists");
        }
    }

    @Override
    public Optional<MainServerDTO> setMainServerScanInterval(String mainServerId, Long scanInterval, boolean overrideCustomInterval, MainServerService mainServerService, SubServerService subServerService, StreamService streamService) {
        Optional<MainServerDTO> mainServerDTO = findOne(mainServerId);
        mainServerDTO.ifPresent(mainServerDTO1 -> {
//            if (!mainServerDTO1.getScanInterval().equals(scanInterval)) {
                mainServerDTO1.setScanInterval(scanInterval);
                save(mainServerDTO1);
                manageSchedule.restartMainServerJob(mainServerDTO1, mainServerService, subServerService);

                List<SubServerDTO> subServerDTOList = subServerService.findByMainServer(mainServerId);

                Long scanInterval1 = mainServerDTO1.getScanInterval();
                for (SubServerDTO subServerDTO : subServerDTOList) {
                    if (overrideCustomInterval) {
                        customIntervalTrue(subServerDTO, scanInterval1, streamService, subServerService);
                    } else {
                        customIntervalFalse(subServerDTO, scanInterval1, streamService, subServerService);
                    }
                }
//            }
        });
        return mainServerDTO;
    }

    private void customIntervalTrue(SubServerDTO subServerDTO, Long scanInterval1, StreamService streamService, SubServerService subServerService) {
        subServerDTO.setInherentInterval(scanInterval1);
        subServerDTO.setCustomInterval(null);
        SubServerDTO savedSubServer = subServerService.save(subServerDTO);
        if (savedSubServer != null) {
            List<StreamDTO> streamDTOList = streamService.findBySubServer(savedSubServer.getId());
            for (StreamDTO streamDTO : streamDTOList) {
                streamDTO.setInherentInterval(scanInterval1);
                streamDTO.setCustomInterval(null);
                StreamDTO savedStream = streamService.save(streamDTO);
                manageSchedule.restartStreamJob(savedStream, streamService);
            }
            manageSchedule.restartSubServer(savedSubServer, subServerService, streamService);
        }
    }

    private void customIntervalFalse(SubServerDTO subServerDTO, Long scanInterval1, StreamService streamService, SubServerService subServerService) {
        if (subServerDTO.getCustomInterval() == null) {
            subServerDTO.setInherentInterval(scanInterval1);
            SubServerDTO savedSubServer = subServerService.save(subServerDTO);
            if (savedSubServer != null) {
                List<StreamDTO> streamDTOList = streamService.findAllByInherentIntervalIsNotNullAndSubServerEquals(savedSubServer);
                for (StreamDTO streamDTO : streamDTOList) {
                    streamDTO.setInherentInterval(scanInterval1);
                    StreamDTO savedStream = streamService.save(streamDTO);
                    manageSchedule.restartStreamJob(savedStream, streamService);
                }
                manageSchedule.restartSubServer(savedSubServer, subServerService, streamService);
            }
        }
    }


    @Override
    public List<MainServerDTO> findAll() {
        List<MainServer> mainServer = mainServerRepository.findAllByIdNotNull();
        return mainServerMapper.toDto(mainServer);
    }


    /**
     * Get all the mainServers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<MainServerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MainServers");
        return mainServerRepository.findAll(pageable)
            .map(mainServerMapper::toDto);
    }


    /**
     * Get one mainServer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<MainServerDTO> findOne(String id) {
        log.debug("Request to get mainServer : {}", id);
        return mainServerRepository.findById(id)
            .map(mainServerMapper::toDto);
    }

    /**
     * Delete the mainServer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete mainServer : {}", id);
        mainServerRepository.deleteById(id);
        mainServerSearchRepository.deleteById(id);
    }

    @Override
    public void deleteMainServerWithAllSubServer(Optional<MainServerDTO> mainServerDTO, SubServerService subServerService, StreamService streamService) {
        mainServerDTO.ifPresent(mainServerDTO1 -> {
            MainServer mainServer = mainServerMapper.toEntity(mainServerDTO1);
            List<SubServerDTO> serverDTOS = subServerService.deleteAllByMainServer(mainServer);
            for (SubServerDTO subServerDTO : serverDTOS) {
                manageSchedule.deleteSubServer(subServerDTO);
            }
            List<StreamDTO> streamList = streamService.deleteAllBySubServerIn(serverDTOS);
            for (StreamDTO streamDTO : streamList) {
                manageSchedule.deleteStreamJob(streamDTO);
            }
            scanArchiveService.deleteAllByStreamIn(streamList);
            manageSchedule.deleteMainServerJob(mainServerMapper.toDto(mainServer));
            delete(mainServer.getId());
        });
    }

    @Override
    public Optional<MainServerDTO> setEmailTrigger(String mainServerId, String emailTrigger, boolean overrideCustomTrigger, List<String> emailUserIds, SubServerService subServerService, StreamService streamService) {
        Optional<MainServerDTO> mainServerDTO = findOne(mainServerId);
        mainServerDTO.ifPresent(mainServerDTO1 -> {
//            if (!mainServerDTO1.getEmailTrigger().equalsIgnoreCase(emailTrigger) || mainServerDTO1.getEmailUserIds() != emailUserIds) {

                for (EmailTrigger trigger : EmailTrigger.values()) {
                    if (trigger.getValue().equalsIgnoreCase(emailTrigger)) {
                        mainServerDTO1.setEmailTrigger(trigger.name());
                    }
                }
                if (mainServerDTO1.getEmailTrigger() != null && emailUserIds != null)
                    mainServerDTO1.setEmailUserIds(emailUserIds);
                if (EmailTrigger.NO_ACTION.getValue().equalsIgnoreCase(mainServerDTO1.getEmailTrigger()))
                    mainServerDTO1.setEmailUserIds(null);
                MainServerDTO saveMainServerDTO = save(mainServerDTO1);
                if (saveMainServerDTO != null) {
                    List<SubServerDTO> subServerByMainServer = subServerService.findByMainServer(saveMainServerDTO.getId());
                    for (SubServerDTO subServerDTO : subServerByMainServer) {
                        if (overrideCustomTrigger) {
                            overrideCustomTriggerTrue(subServerDTO, saveMainServerDTO, subServerService, streamService);
                        } else {
                            overrideCustomTriggerFalse(subServerDTO, saveMainServerDTO, subServerService, streamService);
                        }
                    }
                }
//            }
        });
        return mainServerDTO;
    }

    private void overrideCustomTriggerTrue(SubServerDTO subServerDTO, MainServerDTO saveMainServerDTO, SubServerService subServerService, StreamService streamService) {
        subServerDTO.setInherentEmailTrigger(saveMainServerDTO.getEmailTrigger());
        subServerDTO.setCustomEmailTrigger(null);
        subServerDTO.setEmailUserIds(saveMainServerDTO.getEmailUserIds());
        SubServerDTO savedSubServer = subServerService.save(subServerDTO);
        List<StreamDTO> streamDTOSBySubServer = streamService.findBySubServer(savedSubServer.getId());
        for (StreamDTO streamDTO : streamDTOSBySubServer) {
            streamDTO.setInherentEmailTrigger(saveMainServerDTO.getEmailTrigger());
            streamDTO.setEmailUserIds(saveMainServerDTO.getEmailUserIds());
            streamDTO.setCustomEmailTrigger(null);
            StreamDTO savedStream = streamService.save(streamDTO);
            if (savedStream != null)
                manageSchedule.restartStreamJob(savedStream, streamService);
        }
    }

    private void overrideCustomTriggerFalse(SubServerDTO subServerDTO, MainServerDTO saveMainServerDTO, SubServerService subServerService, StreamService streamService) {
        if (subServerDTO.getCustomEmailTrigger() == null) {
            subServerDTO.setInherentEmailTrigger(saveMainServerDTO.getEmailTrigger());
            subServerDTO.setEmailUserIds(saveMainServerDTO.getEmailUserIds());

            SubServerDTO savedSubServer = subServerService.save(subServerDTO);
            List<StreamDTO> streamDTOSBySubServer = streamService.findBySubServer(savedSubServer.getId());
            for (StreamDTO streamDTO : streamDTOSBySubServer) {
                if (streamDTO.getCustomEmailTrigger() == null) {
                    streamDTO.setInherentEmailTrigger(saveMainServerDTO.getEmailTrigger());
                    streamDTO.setEmailUserIds(saveMainServerDTO.getEmailUserIds());
                    StreamDTO savedStream = streamService.save(streamDTO);
                    if (savedStream != null)
                        manageSchedule.restartStreamJob(savedStream, streamService);
                }
            }
        }
    }

    /**
     * Search for the mainServer corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<MainServerDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MainServers for query {}", query);
        return mainServerSearchRepository.search(queryStringQuery(query), pageable)
            .map(mainServerMapper::toDto);
    }
}
