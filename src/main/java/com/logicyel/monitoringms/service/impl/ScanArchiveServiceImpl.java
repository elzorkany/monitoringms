package com.logicyel.monitoringms.service.impl;

import com.logicyel.monitoringms.domain.ScanArchive;
import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.repository.ScanArchiveRepository;
import com.logicyel.monitoringms.repository.search.ScanArchiveSearchRepository;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.mapper.ScanArchiveMapper;
import com.logicyel.monitoringms.service.mapper.StreamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing ScanArchive.
 */
@Service
public class ScanArchiveServiceImpl implements ScanArchiveService {

    private final Logger log = LoggerFactory.getLogger(ScanArchiveServiceImpl.class);

    private final ScanArchiveRepository scanArchiveRepository;

    private final ScanArchiveMapper scanArchiveMapper;

    private final ScanArchiveSearchRepository scanArchiveSearchRepository;

    private final StreamMapper streamMapper;

    public ScanArchiveServiceImpl(ScanArchiveRepository scanArchiveRepository, ScanArchiveMapper scanArchiveMapper, ScanArchiveSearchRepository scanArchiveSearchRepository, StreamMapper streamMapper) {
        this.scanArchiveRepository = scanArchiveRepository;
        this.scanArchiveMapper = scanArchiveMapper;
        this.scanArchiveSearchRepository = scanArchiveSearchRepository;

        this.streamMapper = streamMapper;
    }

    /**
     * Save a scanArchive.
     *
     * @param scanArchiveDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ScanArchiveDTO save(ScanArchiveDTO scanArchiveDTO) {
        log.debug("Request to save ScanArchive : {}", scanArchiveDTO);
        ScanArchive scanArchive = scanArchiveMapper.toEntity(scanArchiveDTO);
        scanArchive = scanArchiveRepository.save(scanArchive);
        ScanArchiveDTO result = scanArchiveMapper.toDto(scanArchive);
        scanArchiveSearchRepository.save(scanArchive);
        return result;
    }


    /**
     * Get all the scanArchives.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ScanArchiveDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ScanArchives");
        return scanArchiveRepository.findAll(pageable)
            .map(scanArchiveMapper::toDto);
    }


    /**
     * Get one scanArchive by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<ScanArchiveDTO> findOne(String id) {
        log.debug("Request to get ScanArchive : {}", id);
        return scanArchiveRepository.findById(id)
            .map(scanArchiveMapper::toDto);
    }

    /**
     * Delete the scanArchive by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ScanArchive : {}", id);
        scanArchiveRepository.deleteById(id);
        scanArchiveSearchRepository.deleteById(id);
    }


    @Override
    public List<ScanArchive> deleteAllByStreamIn(List<StreamDTO> streamDTOS) {
        List<Stream> streams = streamMapper.toEntity(streamDTOS);
        List<ScanArchive> scanArchives = scanArchiveRepository.deleteAllByStreamIn(streams);
        for (ScanArchive scanArchive : scanArchives) {
            scanArchiveSearchRepository.deleteById(scanArchive.getId());
        }
        return scanArchives;
    }

    @Override
    public List<ScanArchive> deleteAllByStream(Stream stream) {
        List<ScanArchive> scanArchives = scanArchiveRepository.deleteAllByStream(stream);
        for (ScanArchive scanArchive : scanArchives) {
            scanArchiveSearchRepository.deleteById(scanArchive.getId());
        }
        return scanArchives;
    }


    /**
     * Search for the scanArchive corresponding to the query.
     *
     * @param query    the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<ScanArchiveDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ScanArchives for query {}", query);
        return scanArchiveSearchRepository.search(queryStringQuery(query), pageable)
            .map(scanArchiveMapper::toDto);
    }
}
