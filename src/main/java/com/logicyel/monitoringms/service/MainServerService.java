package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.service.dto.MainServerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing mainServer.
 */
public interface MainServerService {

    /**
     * Save a mainServer.
     *
     * @param mainServerDTO the entity to save
     * @return the persisted entity
     */
    MainServerDTO save(MainServerDTO mainServerDTO);
    MainServerDTO save(MainServerDTO mainServerDTO,MainServerService mainServerService,SubServerService subServerService);
    MainServerDTO save(String serverType, String mainServerName, MultipartFile file);
    Optional<MainServerDTO> setMainServerScanInterval(String mainServerId, Long scanInterval,boolean overrideCustomInterval, MainServerService mainServerService, SubServerService subServerService, StreamService streamService);
    List<MainServerDTO> findAll();
    void deleteMainServerWithAllSubServer(Optional<MainServerDTO> mainServerDTO,SubServerService subServerService,StreamService streamService);
    Optional<MainServerDTO> setEmailTrigger(String mainServerId, String emailTrigger, boolean overrideCustomTrigger, List<String> emailUserIds, SubServerService subServerService,StreamService streamService);

    /**
     * Get all the mainServer.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<MainServerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" mainServer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MainServerDTO> findOne(String id);

    /**
     * Delete the "id" mainServer.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the mainServer corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<MainServerDTO> search(String query, Pageable pageable);
}
