package com.logicyel.monitoringms.service;

import com.logicyel.monitoringms.storage.StorageException;
import com.logicyel.monitoringms.storage.StorageService;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Configuration
public class UploadService {
    private final StorageService storageService;

    public UploadService(StorageService storageService) {
        this.storageService = storageService;
    }

    public File uploadFile(MultipartFile file) throws StorageException
    {
        return storageService.store(file);
    }
}
