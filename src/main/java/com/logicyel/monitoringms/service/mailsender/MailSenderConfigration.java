package com.logicyel.monitoringms.service.mailsender;

import com.logicyel.monitoringms.service.EmailConfigService;
import com.logicyel.monitoringms.service.dto.EmailConfigDTO;
import com.logicyel.monitoringms.web.rest.MainServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailSenderConfigration {

    private final Logger log = LoggerFactory.getLogger(MainServerResource.class);

    private String emailFrom;

    @Autowired
    private EmailConfigService emailConfigService;

    @Bean
    public JavaMailSender getJavaMailSender() {

        EmailConfigDTO emailConfig = getEmailConfig();
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        if (emailConfig != null) {
            mailSender.setHost(emailConfig.getHost());
            mailSender.setPort(emailConfig.getPort());
            mailSender.setProtocol(emailConfig.getProtocol());

            mailSender.setUsername(emailConfig.getUsername());
            setEmailFrom(emailConfig.getUsername());
            mailSender.setPassword(emailConfig.getPassword());

            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", emailConfig.getProtocol());
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.tls", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.debug", "true");
            props.put("mail.smtp.ssl.enable", "true");
            props.put("ssl.trust", emailConfig.getHost());


            return mailSender;
        } else {
            log.error("the mail configuration is not set right !!");
        }
        return new JavaMailSenderImpl();
    }

    String getEmailFrom() {
        return emailFrom;
    }

    private void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    private EmailConfigDTO getEmailConfig() {
        return emailConfigService.findLastEmailConfig();
    }
}
