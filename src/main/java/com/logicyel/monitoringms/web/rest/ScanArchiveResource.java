package com.logicyel.monitoringms.web.rest;
import com.logicyel.monitoringms.service.ScanArchiveService;
import com.logicyel.monitoringms.service.dto.ScanArchiveDTO;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import com.logicyel.monitoringms.web.rest.util.HeaderUtil;
import com.logicyel.monitoringms.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ScanArchive.
 */
@RestController
@RequestMapping("/api")
public class ScanArchiveResource {

    private final Logger log = LoggerFactory.getLogger(ScanArchiveResource.class);

    private static final String ENTITY_NAME = "monitoringmsScanArchive";

    private final ScanArchiveService scanArchiveService;

    public ScanArchiveResource(ScanArchiveService scanArchiveService) {
        this.scanArchiveService = scanArchiveService;
    }

    /**
     * POST  /scan-archives : Create a new scanArchive.
     *
     * @param scanArchiveDTO the scanArchiveDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new scanArchiveDTO, or with status 400 (Bad Request) if the scanArchive has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/scan-archives")
    public ResponseEntity<ScanArchiveDTO> createScanArchive(@RequestBody ScanArchiveDTO scanArchiveDTO) throws URISyntaxException {
        log.debug("REST request to save ScanArchive : {}", scanArchiveDTO);
        if (scanArchiveDTO.getId() != null) {
            throw new BadRequestAlertException("A new scanArchive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ScanArchiveDTO result = scanArchiveService.save(scanArchiveDTO);
        return ResponseEntity.created(new URI("/api/scan-archives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /scan-archives : Updates an existing scanArchive.
     *
     * @param scanArchiveDTO the scanArchiveDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated scanArchiveDTO,
     * or with status 400 (Bad Request) if the scanArchiveDTO is not valid,
     * or with status 500 (Internal Server Error) if the scanArchiveDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/scan-archives")
    public ResponseEntity<ScanArchiveDTO> updateScanArchive(@RequestBody ScanArchiveDTO scanArchiveDTO) throws URISyntaxException {
        log.debug("REST request to update ScanArchive : {}", scanArchiveDTO);
        if (scanArchiveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ScanArchiveDTO result = scanArchiveService.save(scanArchiveDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, scanArchiveDTO.getId()))
            .body(result);
    }

    /**
     * GET  /scan-archives : get all the scanArchives.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of scanArchives in body
     */
    @GetMapping("/scan-archives")
    public ResponseEntity<List<ScanArchiveDTO>> getAllScanArchives(Pageable pageable) {
        log.debug("REST request to get a page of ScanArchives");
        Page<ScanArchiveDTO> page = scanArchiveService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/scan-archives");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /scan-archives/:id : get the "id" scanArchive.
     *
     * @param id the id of the scanArchiveDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the scanArchiveDTO, or with status 404 (Not Found)
     */
    @GetMapping("/scan-archives/{id}")
    public ResponseEntity<ScanArchiveDTO> getScanArchive(@PathVariable String id) {
        log.debug("REST request to get ScanArchive : {}", id);
        Optional<ScanArchiveDTO> scanArchiveDTO = scanArchiveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(scanArchiveDTO);
    }

    /**
     * DELETE  /scan-archives/:id : delete the "id" scanArchive.
     *
     * @param id the id of the scanArchiveDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/scan-archives/{id}")
    public ResponseEntity<Void> deleteScanArchive(@PathVariable String id) {
        log.debug("REST request to delete ScanArchive : {}", id);
        scanArchiveService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/scan-archives?query=:query : search for the scanArchive corresponding
     * to the query.
     *
     * @param query the query of the scanArchive search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/scan-archives")
    public ResponseEntity<List<ScanArchiveDTO>> searchScanArchives(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ScanArchives for query {}", query);
        Page<ScanArchiveDTO> page = scanArchiveService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/scan-archives");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
