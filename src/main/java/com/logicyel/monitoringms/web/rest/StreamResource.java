package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.model.StreamCategory;
import com.logicyel.monitoringms.model.StreamImage;
import com.logicyel.monitoringms.model.StreamStatus;
import com.logicyel.monitoringms.model.StreamsModel;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.StreamDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import com.logicyel.monitoringms.web.rest.util.HeaderUtil;
import com.logicyel.monitoringms.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


/**
 * REST controller for managing Stream.
 */
@RestController
@RequestMapping("/api")
public class StreamResource {

    private final Logger log = LoggerFactory.getLogger(StreamResource.class);

    private static final String ENTITY_NAME = "monitoringmsStream";

    private final StreamService streamService;
    private final SubServerService subServerService;

    public StreamResource(StreamService streamService, SubServerService subServerService) {
        this.streamService = streamService;
        this.subServerService = subServerService;
    }

    /**
     * POST  /streams : Create a new stream.
     *
     * @param streamDTO the streamDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new streamDTO, or with status 400 (Bad Request) if the stream has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/streams")
    public ResponseEntity<StreamDTO> createStream(@RequestBody StreamDTO streamDTO) throws URISyntaxException {
        log.debug("REST request to save Stream : {}", streamDTO);
        if (streamDTO.getId() != null) {
            throw new BadRequestAlertException("A new stream cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StreamDTO result = streamService.save(streamDTO);
        return ResponseEntity.created(new URI("/api/streams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /streams : Updates an existing stream.
     *
     * @param streamDTO the streamDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated streamDTO,
     * or with status 400 (Bad Request) if the streamDTO is not valid,
     * or with status 500 (Internal Server Error) if the streamDTO couldn't be updated
     */
    @PutMapping("/streams")
    public ResponseEntity<StreamDTO> updateStream(@RequestBody StreamDTO streamDTO) {
        log.debug("REST request to update Stream : {}", streamDTO);
        if (streamDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StreamDTO result = streamService.save(streamDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, streamDTO.getId()))
            .body(result);
    }

    /**
     * GET  /streams : get all the streams.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of streams in body
     */
    @GetMapping("/streams")
    public ResponseEntity<List<StreamDTO>> getAllStreams(Pageable pageable) {
        log.debug("REST request to get a page of Streams");
        Page<StreamDTO> page = streamService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/streams");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /streams/:id : get the "id" stream.
     *
     * @param id the id of the streamDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the streamDTO, or with status 404 (Not Found)
     */
    @GetMapping("/streams/{id}")
    public ResponseEntity<StreamDTO> getStream(@PathVariable String id) {
        log.debug("REST request to get Stream : {}", id);
        Optional<StreamDTO> streamDTO = streamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(streamDTO);
    }

    /**
     * DELETE  /streams/:id : delete the "id" stream.
     *
     * @param id the id of the streamDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/streams/{id}")
    public ResponseEntity<Void> deleteStream(@PathVariable String id) {
        log.debug("REST request to delete Stream : {}", id);
        streamService.deleteAllScanArchiveByStream(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/streams?query=:query : search for the stream corresponding
     * to the query.
     *
     * @param query    the query of the stream search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/streams")
    public ResponseEntity<List<StreamDTO>> searchStreams(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Streams for query {}", query);
        Page<StreamDTO> page = streamService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/streams");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /get-stream-image : get the streamImage.
     *
     * @return the ResponseEntity with status 200 (OK) and , or with status 404 (Not Found)
     */
    @GetMapping(value = "/get-stream-image")
    public ResponseEntity<List<StreamImage>> getStreamImage(Pageable pageable) throws IOException, InterruptedException {
        Page<StreamImage> streamImages = streamService.streamImage(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(streamImages, "/get-stream-image");
        return ResponseEntity.ok().headers(headers).body(streamImages.getContent());
    }


    /**
     * POST  /create-streams : Create a new subServers.
     *
     * @param streamDTOS  streamOfsubServer list
     * @param subServerId the id of subserver
     * @return the ResponseEntity with status 201 (Created) and with body the new streamDTO
     */
    @PostMapping("/create-streams")
    public ResponseEntity<List<StreamDTO>> createStreams(@RequestBody List<StreamDTO> streamDTOS, @RequestParam("subServerId") String subServerId) {
        log.debug("REST request to save SubServer : {}", streamDTOS);
        List<StreamDTO> result = streamService.createListStreamServer(streamDTOS, subServerId, subServerService, streamService);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    /**
     * GET  /discover-sub-server/:subServerId : get the channels-by-server.
     *
     * @param subServerId the id of the subServer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found) and 606 NOT_ACCEPTABLE (internal server error)
     */
    @GetMapping("/discover-sub-server/{subServerId}")
    public ResponseEntity<StreamCategory> discoverSubServer(@PathVariable String subServerId) {
        log.debug("REST request to search for a page of subServers for query {}", subServerId);
        Optional<SubServerDTO> subServerDTO = subServerService.findOne(subServerId);
        if (!subServerDTO.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<StreamsModel> streamsModels;
        List<StreamDTO> streamDTO;
        List streamInSystemButDeleted;
        try {
            streamsModels = streamService.scanSubServer(subServerDTO);
            streamDTO = streamService.findBySubServer(subServerId);
            streamInSystemButDeleted = streamService.streamInSystemButDeleted(streamsModels, subServerId);

        } catch (Throwable e) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        StreamCategory streamCategory = new StreamCategory(streamDTO, streamsModels, streamInSystemButDeleted);

        return new ResponseEntity<>(streamCategory, HttpStatus.OK);
    }

    /**
     * GET  /get-stream-by-sub/:streamId : get the "id" subServer.
     *
     * @param subServerId the id of the subServer to retrieve Streams
     * @return the ResponseEntity with status 200 (OK) and with body the Streams, or with status 404 (Not Found)
     */
    @GetMapping("/get-stream-by-sub-server/{subServerId}")
    public ResponseEntity<List<StreamDTO>> getStreamsBySubServer(@PathVariable String subServerId, Pageable pageable) {
        log.debug("REST request to get-subServer-by-mainServer  {}", subServerId);
        Page<StreamDTO> streamDTOS = streamService.findBySubServerPageable(subServerId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(streamDTOS, "/stream-status");
        return ResponseEntity.ok().headers(headers).body(streamDTOS.getContent());

    }

    /**
     * GET  /stream-status : get all getStreamStatus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of getStreamStatus in body
     */
    @GetMapping("/stream-status")
    public ResponseEntity<List<StreamStatus>> streamStatus(Pageable pageable) throws InterruptedException {

        Page<StreamStatus> streamStatuses = streamService.getStreamStatus(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(streamStatuses, "/stream-status");
        return ResponseEntity.ok().headers(headers).body(streamStatuses.getContent());
    }

    /**
     * GET  /stream-custom-interval/:streamId/:customInterval : get the "id" and "customInterval" subServer.
     *
     * @param streamId       the id of the subServerDTO to retrieve
     * @param customInterval the scanInterval of the subServer in ms
     * @return the ResponseEntity with status 200 (OK) and with body the streamDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stream-custom-interval/{streamId}/{customInterval}")
    public ResponseEntity<StreamDTO> setStreamCustomInterval(@PathVariable String streamId, @PathVariable Long customInterval) {
        log.debug("REST request to get mainServer : {}", streamId);
        Optional<StreamDTO> streamDTO = streamService.setStreamCustomInterval(streamId, customInterval, streamService);
        return ResponseUtil.wrapOrNotFound(streamDTO);
    }

    /**
     * POST  /stream-custom-email-trigger .
     *
     * @param streamId                   the id of the streamDTO to retrieve
     * @param customEmailTrigger         the action customEmailTrigger of the subServer
     * @param emailUserIds               the ids of users
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @PostMapping("/stream-custom-email-trigger")
    public ResponseEntity<StreamDTO> setCustomEmailTrigger(@RequestParam("streamId") String streamId, @RequestParam("customEmailTrigger") String customEmailTrigger,@RequestParam(value = "emailUserIds",required = false) List<String> emailUserIds) {
        log.debug("REST request to get stream : {}", streamId);
        Optional<StreamDTO> streamDTO = streamService.setCustomEmailTrigger(streamId, customEmailTrigger, emailUserIds,streamService);
        return ResponseUtil.wrapOrNotFound(streamDTO);
    }
}

