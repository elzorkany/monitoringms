package com.logicyel.monitoringms.web.rest;
import com.logicyel.monitoringms.service.EmailConfigService;
import com.logicyel.monitoringms.service.dto.EmailConfigDTO;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import com.logicyel.monitoringms.web.rest.util.HeaderUtil;
import com.logicyel.monitoringms.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EmailConfig.
 */
@RestController
@RequestMapping("/api")
public class EmailConfigResource {

    private final Logger log = LoggerFactory.getLogger(EmailConfigResource.class);

    private static final String ENTITY_NAME = "monitoringmsEmailConfig";

    private final EmailConfigService emailConfigService;

    public EmailConfigResource(EmailConfigService emailConfigService) {
        this.emailConfigService = emailConfigService;
    }

    /**
     * POST  /email-configs : Create a new emailConfig.
     *
     * @param emailConfigDTO the emailConfigDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new emailConfigDTO, or with status 400 (Bad Request) if the emailConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/email-configs")
    public ResponseEntity<EmailConfigDTO> createEmailConfig(@RequestBody EmailConfigDTO emailConfigDTO) throws URISyntaxException {
        log.debug("REST request to save EmailConfig : {}", emailConfigDTO);
        if (emailConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new emailConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmailConfigDTO result = emailConfigService.save(emailConfigDTO);
        return ResponseEntity.created(new URI("/api/email-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /email-configs : Updates an existing emailConfig.
     *
     * @param emailConfigDTO the emailConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated emailConfigDTO,
     * or with status 400 (Bad Request) if the emailConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the emailConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/email-configs")
    public ResponseEntity<EmailConfigDTO> updateEmailConfig(@RequestBody EmailConfigDTO emailConfigDTO) throws URISyntaxException {
        log.debug("REST request to update EmailConfig : {}", emailConfigDTO);
        if (emailConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EmailConfigDTO result = emailConfigService.save(emailConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, emailConfigDTO.getId()))
            .body(result);
    }

    /**
     * GET  /email-configs : get all the emailConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of emailConfigs in body
     */
    @GetMapping("/email-configs")
    public ResponseEntity<List<EmailConfigDTO>> getAllEmailConfigs(Pageable pageable) {
        log.debug("REST request to get a page of EmailConfigs");
        Page<EmailConfigDTO> page = emailConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/email-configs");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /email-configs/:id : get the "id" emailConfig.
     *
     * @param id the id of the emailConfigDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the emailConfigDTO, or with status 404 (Not Found)
     */
    @GetMapping("/email-configs/{id}")
    public ResponseEntity<EmailConfigDTO> getEmailConfig(@PathVariable String id) {
        log.debug("REST request to get EmailConfig : {}", id);
        Optional<EmailConfigDTO> emailConfigDTO = emailConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(emailConfigDTO);
    }

    /**
     * DELETE  /email-configs/:id : delete the "id" emailConfig.
     *
     * @param id the id of the emailConfigDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/email-configs/{id}")
    public ResponseEntity<Void> deleteEmailConfig(@PathVariable String id) {
        log.debug("REST request to delete EmailConfig : {}", id);
        emailConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/email-configs?query=:query : search for the emailConfig corresponding
     * to the query.
     *
     * @param query the query of the emailConfig search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/email-configs")
    public ResponseEntity<List<EmailConfigDTO>> searchEmailConfigs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of EmailConfigs for query {}", query);
        Page<EmailConfigDTO> page = emailConfigService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/email-configs");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
