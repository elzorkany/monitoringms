/**
 * View Models used by Spring MVC REST controllers.
 */
package com.logicyel.monitoringms.web.rest.vm;
