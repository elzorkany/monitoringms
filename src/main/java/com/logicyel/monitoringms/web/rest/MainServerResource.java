package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.StreamService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import com.logicyel.monitoringms.web.rest.util.HeaderUtil;
import com.logicyel.monitoringms.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


/**
 * REST controller for managing mainServer.
 */
@RestController
@RequestMapping("/api")
public class MainServerResource {

    private final Logger log = LoggerFactory.getLogger(MainServerResource.class);

    private static final String ENTITY_NAME = "monitoringmsMainServer";

    private final MainServerService mainServerService;
    private final SubServerService subServerService;
    private final StreamService streamService;

    public MainServerResource(MainServerService mainServerService, SubServerService subServerService, StreamService streamService) {
        this.mainServerService = mainServerService;
        this.subServerService = subServerService;

        this.streamService = streamService;
    }

    /**
     * POST  /main-servers : Create a new mainServer.
     *
     * @param mainServerDTO the mainServerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mainServerDTO, or with status 400 (Bad Request) if the mainServer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/main-servers")
    public ResponseEntity<MainServerDTO> createMainServer(@RequestBody MainServerDTO mainServerDTO) throws URISyntaxException {
        log.debug("REST request to save mainServer : {}", mainServerDTO);
        if (mainServerDTO.getId() != null) {
            throw new BadRequestAlertException("A new mainServer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String baseUrl = mainServerDTO.getBaseUrl();
        if (baseUrl != null && baseUrl.length() > 0 && baseUrl.charAt(baseUrl.length() - 1) == '/') {
            baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
        }
        mainServerDTO.setBaseUrl(baseUrl);

        MainServerDTO result = mainServerService.save(mainServerDTO, mainServerService, subServerService);
        return ResponseEntity.created(new URI("/api/main-servers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /main-servers : Updates an existing mainServer.
     *
     * @param mainServerDTO the mainServerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mainServerDTO,
     * or with status 400 (Bad Request) if the mainServerDTO is not valid,
     * or with status 500 (Internal Server Error) if the mainServerDTO couldn't be updated
     */
    @PutMapping("/main-servers")
    public ResponseEntity<MainServerDTO> updateMainServer(@RequestBody MainServerDTO mainServerDTO) {
        log.debug("REST request to update mainServer : {}", mainServerDTO);
        if (mainServerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MainServerDTO result = mainServerService.save(mainServerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, mainServerDTO.getId()))
            .body(result);
    }

    /**
     * GET  /main-servers : get all the MainServers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of MainServers in body
     */
    @GetMapping("/main-servers")
    public ResponseEntity<List<MainServerDTO>> getAllMainServers(Pageable pageable) {
        log.debug("REST request to get a page of MainServers");
        Page<MainServerDTO> page = mainServerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/main-servers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /main-servers/:id : get the "id" mainServer.
     *
     * @param id the id of the mainServerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mainServerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/main-servers/{id}")
    public ResponseEntity<MainServerDTO> getMainServer(@PathVariable String id) {
        log.debug("REST request to get mainServer : {}", id);
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mainServerDTO);
    }

    /**
     * DELETE  /main-servers/:id : delete the "id" mainServer.
     *
     * @param id the id of the mainingServerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/main-servers/{id}")
    public ResponseEntity<Void> deleteMainServerServer(@PathVariable String id) {
        log.debug("REST request to delete mainServer : {}", id);
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(id);
        if (mainServerDTO.isPresent())
            mainServerService.deleteMainServerWithAllSubServer(mainServerDTO, subServerService, streamService);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/main-servers?query=:query : search for the mainServer corresponding
     * to the query.
     *
     * @param query    the query of the mainServer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/main-servers")
    public ResponseEntity<List<MainServerDTO>> searchMainServers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of MainServers for query {}", query);
        Page<MainServerDTO> page = mainServerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/main-servers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * POST  /main-servers : Create a new mainServer.
     *
     * @param serverType     the type of mainserver
     * @param mainServerName the name of maniServer
     * @param file           the m3u file
     * @return the ResponseEntity with status 200 (OK) and with body the mainServerDto, or with status 404 (Not Found)
     */
    @PostMapping("/main-server")
    public ResponseEntity<MainServerDTO> createMainServer(@RequestParam("serverType") String serverType, @RequestParam("mainServerName") String mainServerName, @RequestParam("file") MultipartFile file) {
        MainServerDTO mainServerDTO;
        try {
            mainServerDTO = mainServerService.save(serverType, mainServerName, file);
            if (mainServerDTO != null)
                subServerService.save(mainServerDTO, file);
        } catch (Exception e) {
            throw new BadRequestAlertException("Failed to create Main server !", ENTITY_NAME, " mainServer not created");
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME
            , file.getOriginalFilename())).body(mainServerDTO);

    }

    /**
     * GET  /main-servers-scan-interval/:mainServerId : get the "id" mainServer.
     *
     * @param mainServerId           the id of the mainServerDTO to retrieve
     * @param scanInterval           the scanInterval of the mainServer in ms
     * @param overrideCustomInterval the T | F to override custome interval
     * @return the ResponseEntity with status 200 (OK) and with body the mainServerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/main-servers-scan-interval/{mainServerId}/{scanInterval}/{overrideCustomInterval}")
    public ResponseEntity<MainServerDTO> setMainServerScanInterval(@PathVariable String mainServerId, @PathVariable Long scanInterval, @PathVariable boolean overrideCustomInterval) {
        log.debug("REST request to get mainServer : {}", mainServerId);
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(mainServerId);
        if (!mainServerDTO.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Optional<MainServerDTO> serverDTOOptional= Optional.of(new MainServerDTO());
        if (scanInterval >= 60L)
            serverDTOOptional = mainServerService.setMainServerScanInterval(mainServerId, scanInterval, overrideCustomInterval, mainServerService, subServerService, streamService);
        return ResponseUtil.wrapOrNotFound(serverDTOOptional);
    }

    /**
     * POST  /set-email-trigger/:mainServerId/:emailTrigger /:overrideCustomTrigger : get the "id" subServer.
     *
     * @param mainServerId          the id of the mainServerDTO to retrieve
     * @param emailTrigger          the action emailTrigger of the subServer
     * @param overrideCustomTrigger the T | F to override custome Trigger
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @PostMapping("/set-email-trigger")
    public ResponseEntity<MainServerDTO> setEmailTrigger(@RequestParam("mainServerId") String mainServerId, @RequestParam("emailTrigger") String emailTrigger, @RequestParam("overrideCustomTrigger") boolean overrideCustomTrigger, @RequestParam(value = "emailUserIds", required = false) List<String> emailUserIds) {
        log.debug("REST request to get subServer : {}", mainServerId);
        Optional<MainServerDTO> mainServerDTO = mainServerService.setEmailTrigger(mainServerId, emailTrigger, overrideCustomTrigger, emailUserIds, subServerService, streamService);
        return ResponseUtil.wrapOrNotFound(mainServerDTO);
    }
}
