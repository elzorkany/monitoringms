package com.logicyel.monitoringms.web.rest;

import com.logicyel.monitoringms.model.SubServerCategory;
import com.logicyel.monitoringms.model.SubServerModel;
import com.logicyel.monitoringms.service.MainServerService;
import com.logicyel.monitoringms.service.SubServerService;
import com.logicyel.monitoringms.service.dto.MainServerDTO;
import com.logicyel.monitoringms.service.dto.SubServerDTO;
import com.logicyel.monitoringms.web.rest.errors.BadRequestAlertException;
import com.logicyel.monitoringms.web.rest.util.HeaderUtil;
import com.logicyel.monitoringms.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubServer.
 */
@RestController
@RequestMapping("/api")
public class SubServerResource {

    private final Logger log = LoggerFactory.getLogger(SubServerResource.class);

    private static final String ENTITY_NAME = "monitoringmsSubServer";

    private final SubServerService subServerService;
    private final MainServerService mainServerService;

    public SubServerResource(SubServerService subServerService, MainServerService mainServerService) {
        this.subServerService = subServerService;
        this.mainServerService = mainServerService;
    }

    /**
     * POST  /sub-servers : Create a new subServer.
     *
     * @param subServerDTO the subServerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subServerDTO, or with status 400
     * (Bad Request) if the subServer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sub-servers")
    public ResponseEntity<SubServerDTO> createSubServer(@RequestBody SubServerDTO subServerDTO) throws URISyntaxException {
        log.debug("REST request to save SubServer : {}", subServerDTO);
        if (subServerDTO.getId() != null) {
            throw new BadRequestAlertException("A new subServer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(subServerDTO.getMainServerId());

        mainServerDTO.ifPresent(mainServerDTO1 -> {
                if (subServerDTO.getCustomInterval() == null)
                    subServerDTO.setInherentInterval(mainServerDTO1.getScanInterval());
        });

        SubServerDTO result = subServerService.save(subServerDTO, subServerService);
        return ResponseEntity.created(new URI("/api/sub-servers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * POST  /create-sub-servers : Create list of a new subServers.
     *
     * @param mainServerId  the id of main server
     * @param subServerDTOS and the subServerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subServerDTO, or with status 400
     * (Bad Request) if the subServer has already an ID
     */
    @PostMapping("/create-sub-servers")
    public ResponseEntity<List<SubServerDTO>> createSubServers(@RequestBody List<SubServerDTO> subServerDTOS, @RequestParam("mainServerId") String mainServerId) {
        log.debug("REST request to save SubServer : {}", subServerDTOS);
        List<SubServerDTO> result = subServerService.createListSubServer(subServerDTOS,
            mainServerId, subServerService);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    /**
     * PUT  /sub-servers : Updates an existing subServer.
     *
     * @param subServerDTO the subServerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subServerDTO,
     * or with status 400 (Bad Request) if the subServerDTO is not valid,
     * or with status 500 (Internal Server Error) if the subServerDTO couldn't be updated
     */
    @PutMapping("/sub-servers")
    public ResponseEntity<SubServerDTO> updateSubServer(@RequestBody SubServerDTO subServerDTO) {
        log.debug("REST request to update SubServer : {}", subServerDTO);
        if (subServerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubServerDTO result = subServerService.save(subServerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subServerDTO.getId()))
            .body(result);
    }

    /**
     * GET  /sub-servers : get all the subServers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subServers in body
     */
    @GetMapping("/sub-servers")
    public ResponseEntity<List<SubServerDTO>> getAllSubServers(Pageable pageable) {
        log.debug("REST request to get a page of subServers");
        Page<SubServerDTO> page = subServerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sub-servers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /sub-servers/:id : get the "id" subServer.
     *
     * @param id the id of the subServerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sub-servers/{id}")
    public ResponseEntity<SubServerDTO> getSubServer(@PathVariable String id) {
        log.debug("REST request to get SubServer : {}", id);
        Optional<SubServerDTO> subServerDTO = subServerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subServerDTO);
    }

    /**
     * DELETE  /sub-servers/:id : delete the "id" subServer.
     *
     * @param id the id of the subServerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sub-servers/{id}")
    public ResponseEntity<Void> deleteSubServer(@PathVariable String id) {
        log.debug("REST request to delete SubServer : {}", id);
        subServerService.deleteAllStreamsBySubServer(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/sub-servers?query=:query : search for the subServer corresponding
     * to the query.
     *
     * @param query    the query of the subServer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/sub-servers")
    public ResponseEntity<List<SubServerDTO>> searchSubServers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of subServers for query {}", query);
        Page<SubServerDTO> page = subServerService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/sub" +
            "-servers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /discover-main-serve/:mainServerId : get the id mainServer.
     *
     * @param mainServerId the id of the mainServer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found) and 606 NOT_ACCEPTABLE (internal server error)
     */
    @GetMapping("/discover-main-server/{mainServerId}")
    public ResponseEntity<SubServerCategory> discoverMainServer(@PathVariable String mainServerId) {
        log.debug("REST request to discover sub servers {}", mainServerId);
        Optional<MainServerDTO> mainServerDTO = mainServerService.findOne(mainServerId);
        if (!mainServerDTO.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<SubServerDTO> subServerDTO;
        List subServersInSystemButDeleted;
        List<SubServerModel> subServerModels;
        try {
            subServerModels = subServerService.scanSubServers(mainServerId);
            subServerDTO = subServerService.findByMainServer(mainServerId);
            subServersInSystemButDeleted = subServerService.subServersInSystemButDeleted(subServerModels, mainServerId);
        } catch (Throwable e) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        SubServerCategory subServerCategory = new SubServerCategory(subServerDTO,
            subServerModels, subServersInSystemButDeleted);

        return new ResponseEntity<>(subServerCategory, HttpStatus.OK);

    }


    /**
     * GET  /get-sub-by-main/:mainServerId : get the "id" mainServer.
     *
     * @param mainServerId the id of the mainServerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/get-sub-by-main/{mainServerId}")
    public ResponseEntity<List<SubServerDTO>> getSubServersByMainServer(@PathVariable String mainServerId, Pageable pageable) {
        log.debug("REST request to get-subServer-by-mainServer  {}", mainServerId);
        Page<SubServerDTO> subServerDTOS = subServerService.findByMainServerPageable(mainServerId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(subServerDTOS, "/stream-status");
        return ResponseEntity.ok().headers(headers).body(subServerDTOS.getContent());
    }

    /**
     * GET  /sub-servers-custom-interval/:subServerId/:customInterval /:overrideCustomInterval : get the "id" subServer.
     *
     * @param subServerId            the id of the subServerDTO to retrieve
     * @param customInterval         the scanInterval of the subServer in ms
     * @param overrideCustomInterval the T | F to override custome interval
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sub-servers-custom-interval/{subServerId}/{customInterval}/{overrideCustomInterval}")
    public ResponseEntity<SubServerDTO> setSubserverCustomInterval(@PathVariable String subServerId, @PathVariable Long customInterval, @PathVariable boolean overrideCustomInterval) {
        log.debug("REST request to get subServer : {}", subServerId);
        Optional<SubServerDTO> subServerDTO = subServerService.setSubserverCustomInterval(subServerId, customInterval, overrideCustomInterval, subServerService);
        return ResponseUtil.wrapOrNotFound(subServerDTO);
    }

    /**
     * POST  /subserver-custom-email-trigger .
     *
     * @param subServerId                the id of the subServerDTO to retrieve
     * @param customEmailTrigger         the action customEmailTrigger of the subServer
     * @param overrideCustomEmailTrigger the T | F to override custome Email Trigger
     * @param emailUserIds               the ids of users
     * @return the ResponseEntity with status 200 (OK) and with body the subServerDTO, or with status 404 (Not Found)
     */
    @PostMapping("/subserver-custom-email-trigger")
    public ResponseEntity<SubServerDTO> setCustomEmailTrigger(@RequestParam("subServerId") String subServerId, @RequestParam("customEmailTrigger") String customEmailTrigger, @RequestParam("overrideCustomEmailTrigger") boolean overrideCustomEmailTrigger, @RequestParam(value = "emailUserIds", required = false) List<String> emailUserIds) {
        log.debug("REST request to get subServer : {}", subServerId);
        Optional<SubServerDTO> subServerDTO = subServerService.setCustomEmailTrigger(subServerId, customEmailTrigger, overrideCustomEmailTrigger, emailUserIds);
        return ResponseUtil.wrapOrNotFound(subServerDTO);
    }

}
