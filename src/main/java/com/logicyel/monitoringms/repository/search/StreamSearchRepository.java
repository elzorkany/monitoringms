package com.logicyel.monitoringms.repository.search;

import com.logicyel.monitoringms.domain.Stream;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Stream entity.
 */
public interface StreamSearchRepository extends ElasticsearchRepository<Stream, String> {
}
