package com.logicyel.monitoringms.repository.search;

import com.logicyel.monitoringms.domain.MainServer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the mainServer entity.
 */
public interface MainServerSearchRepository extends ElasticsearchRepository<MainServer, String> {
}
