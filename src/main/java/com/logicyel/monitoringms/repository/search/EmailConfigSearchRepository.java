package com.logicyel.monitoringms.repository.search;

import com.logicyel.monitoringms.domain.EmailConfig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the EmailConfig entity.
 */
public interface EmailConfigSearchRepository extends ElasticsearchRepository<EmailConfig, String> {
}
