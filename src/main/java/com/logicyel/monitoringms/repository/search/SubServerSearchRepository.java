package com.logicyel.monitoringms.repository.search;

import com.logicyel.monitoringms.domain.SubServer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SubServer entity.
 */
public interface SubServerSearchRepository extends ElasticsearchRepository<SubServer, String> {
}
