package com.logicyel.monitoringms.repository.search;

import com.logicyel.monitoringms.domain.ScanArchive;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ScanArchive entity.
 */
public interface ScanArchiveSearchRepository extends ElasticsearchRepository<ScanArchive, String> {
}
