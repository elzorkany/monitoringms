package com.logicyel.monitoringms.repository;

import com.logicyel.monitoringms.domain.Stream;
import com.logicyel.monitoringms.domain.SubServer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data MongoDB repository for the Stream entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StreamRepository extends MongoRepository<Stream, String> {

    List<Stream> findBySubServer(String originServerId);
    Page<Stream> findBySubServer(String originServerId, Pageable pageable);
    List<Stream> deleteAllBySubServerIn(List<SubServer> subServers);
    List<Stream> deleteAllBySubServer(SubServer subServers);
    List<Stream> findAllByCustomIntervalIsNullAndSubServerEquals(SubServer subServer);
    List<Stream> findAllByIdNotNull();


}
