package com.logicyel.monitoringms.repository;

import com.logicyel.monitoringms.domain.EmailConfig;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the EmailConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmailConfigRepository extends MongoRepository<EmailConfig, String> {
//    EmailConfig findTopByStreamOrderByIdDesc
    EmailConfig findTopByIdNotNullOrderByIdDesc();
}
