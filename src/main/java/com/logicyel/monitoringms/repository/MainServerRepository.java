package com.logicyel.monitoringms.repository;

import com.logicyel.monitoringms.domain.MainServer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data MongoDB repository for the mainServer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MainServerRepository extends MongoRepository<MainServer, String> {
    List<MainServer> findAllByIdNotNull();

}
