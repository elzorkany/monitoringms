package com.logicyel.monitoringms.repository;

import com.logicyel.monitoringms.domain.ScanArchive;
import com.logicyel.monitoringms.domain.Stream;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data MongoDB repository for the ScanArchive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ScanArchiveRepository extends MongoRepository<ScanArchive, String> {
    List<ScanArchive> deleteAllByStreamIn(List<Stream> streams);
    List<ScanArchive> deleteAllByStream(Stream stream);

}
