package com.logicyel.monitoringms.repository;

import com.logicyel.monitoringms.domain.MainServer;
import com.logicyel.monitoringms.domain.SubServer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data MongoDB repository for the SubServer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubServerRepository extends MongoRepository<SubServer, String> {
    List<SubServer> findByMainServer(String transcodingId);

    Page<SubServer> findByMainServer(String transcodingId, Pageable pageable);

    List<SubServer> findAllByIdNotNull();

    List<SubServer> deleteAllByMainServer(MainServer mainServer);
}
