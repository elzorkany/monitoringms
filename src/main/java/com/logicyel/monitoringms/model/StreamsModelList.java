package com.logicyel.monitoringms.model;

import java.util.ArrayList;
import java.util.List;

public class StreamsModelList {

    private ArrayList<StreamsModel> streamsModels = new ArrayList<>();

    public List<StreamsModel> getStreamsModels() {
        return streamsModels;
    }

    public void setStreamsModels(List<StreamsModel> streamsModels) {
        this.streamsModels = (ArrayList<StreamsModel>) streamsModels;
    }
}
