package com.logicyel.monitoringms.model;

import com.logicyel.monitoringms.service.dto.StreamDTO;

import java.util.ArrayList;
import java.util.List;

public class StreamCategory {
    private List inSystem;
    private List outSystem;
    private List inSystemButDeleted;

    public StreamCategory(List<StreamDTO> streamDTOS, List<StreamsModel> streamsModels, List streamInSystemButDeleted) {

        List<StreamDTO> streamList = convertStreamModelToDomainDTO(streamsModels);


        List<StreamDTO> commonStreams = new ArrayList<>(streamDTOS);

        commonStreams.retainAll(streamList);
        this.setInSystem(commonStreams);

        streamList.removeAll(streamDTOS);
        this.setOutSystem(streamList);

        this.setInSystemButDeleted(streamInSystemButDeleted);

    }

    public static List<StreamDTO> convertStreamModelToDomainDTO(List<StreamsModel> streamsModels) {
        List<StreamDTO> streamDTOS = new ArrayList<>();
        for (StreamsModel Stream : streamsModels) {
            StreamDTO streamDTO = new StreamDTO();

            streamDTO.setIdOnServer(Stream.getChannelID());
            streamDTO.setUrl(Stream.getUrl());
            streamDTO.setName(Stream.getChannelName());
            streamDTOS.add(streamDTO);
        }
        return streamDTOS;
    }

    public List getInSystem() {
        return inSystem;
    }

    public void setInSystem(List inSystem) {
        this.inSystem = inSystem;
    }

    public List getOutSystem() {
        return outSystem;
    }

    public void setOutSystem(List outSystem) {
        this.outSystem = outSystem;
    }

    public List getInSystemButDeleted() {
        return inSystemButDeleted;
    }

    public void setInSystemButDeleted(List inSystemButDeleted) {
        this.inSystemButDeleted = inSystemButDeleted;
    }
}
