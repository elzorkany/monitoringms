package com.logicyel.monitoringms.model;

import java.util.ArrayList;
import java.util.List;

public class SubServerModelList {

    private ArrayList<SubServerModel> subServerModels =new ArrayList<>();

    public SubServerModelList(){

    }
    public SubServerModelList(List<SubServerModel> subServerModels) {
        this.subServerModels = (ArrayList<SubServerModel>) subServerModels;
    }

    public List<SubServerModel> getSubServerModels() {
        return subServerModels;
    }

    public void setSubServerModels(List<SubServerModel> subServerModels) {
        this.subServerModels = (ArrayList<SubServerModel>) subServerModels;
    }
}
