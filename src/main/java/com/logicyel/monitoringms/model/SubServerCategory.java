package com.logicyel.monitoringms.model;

import com.logicyel.monitoringms.service.dto.SubServerDTO;

import java.util.ArrayList;
import java.util.List;

public class SubServerCategory {
    private List inSystem;
    private List outSystem;
    private List inSystemButDeleted;

    public SubServerCategory() {
    }

    public SubServerCategory(List<SubServerDTO> subServerDTOS,
                             List<SubServerModel> subServerModels,
                             List originInSystemButDeleted) {


        List<SubServerDTO> originServerList = convertOriginModelToDomainDTO(subServerModels);

        List<SubServerDTO> commonOrigins = new ArrayList<>(subServerDTOS);

        commonOrigins.retainAll(originServerList);
        this.inSystem = commonOrigins;

        originServerList.removeAll(subServerDTOS);
        this.outSystem = originServerList;

        this.inSystemButDeleted = originInSystemButDeleted;
    }

    public static List<SubServerDTO> convertOriginModelToDomainDTO(List<SubServerModel> subServerModels) {
        List<SubServerDTO> subServerDTOS = new ArrayList<>();
        for (SubServerModel subServerModel : subServerModels) {
            SubServerDTO subServerDTO = new SubServerDTO();
            subServerDTO.setIdOnServer(subServerModel.getServerID());
            subServerDTO.setBaseUrl(subServerModel.getUrl());
            subServerDTO.setName(subServerModel.getServerName());
            subServerDTOS.add(subServerDTO);

        }
        return subServerDTOS;
    }

    public List getInSystem() {
        return inSystem;
    }

    public void setInSystem(List inSystem) {
        this.inSystem = inSystem;
    }

    public List getOutSystem() {
        return outSystem;
    }

    public void setOutSystem(List outSystem) {
        this.outSystem = outSystem;
    }

    public List getInSystemButDeleted() {
        return inSystemButDeleted;
    }

    public void setInSystemButDeleted(List inSystemButDeleted) {
        this.inSystemButDeleted = inSystemButDeleted;
    }
}
