package com.logicyel.monitoringms.model;

import com.logicyel.monitoringms.service.dto.StreamDTO;

import java.util.Arrays;

public class StreamImage extends StreamDTO {
    private byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        StreamImage that = (StreamImage) o;
        return Arrays.equals(image, that.image);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }
}
