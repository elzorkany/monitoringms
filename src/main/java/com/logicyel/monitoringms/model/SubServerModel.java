package com.logicyel.monitoringms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubServerModel {

    private int serverID;
    private String serverName;
    private int channelsCount;
    private String url;
    private String id;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public int getChannelsCount() {
        return channelsCount;
    }

    public void setChannelsCount(int channelsCount) {
        this.channelsCount = channelsCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (!(o instanceof SubServerModel)) return false;
        SubServerModel that = (SubServerModel) o;

        return serverID == that.serverID &&
            Objects.equals(serverName, that.serverName) &&
            Objects.equals(url, that.url);
    }
    @Override
    public int hashCode() {
        return Objects.hash(serverID, serverName, url);
    }
}
