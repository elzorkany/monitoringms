package com.logicyel.monitoringms.model.streamjsonmodel;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "index",
    "codec_name",
    "codec_long_name",
    "profile",
    "codec_type",
    "codec_time_base",
    "codec_tag_string",
    "codec_tag",
    "width",
    "height",
    "coded_width",
    "coded_height",
    "color_range",
    "has_b_frames",
    "sample_aspect_ratio",
    "display_aspect_ratio",
    "pix_fmt",
    "level",
    "chroma_location",
    "field_order",
    "refs",
    "is_avc",
    "nal_length_size",
    "id",
    "r_frame_rate",
    "avg_frame_rate",
    "time_base",
    "start_pts",
    "start_time",
    "bits_per_raw_sample",
    "sample_fmt",
    "sample_rate",
    "channels",
    "channel_layout",
    "bits_per_sample",
    "bit_rate",
    "disposition",
    "tags",
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Streams {

    @JsonProperty("index")
    private Integer index;
    @JsonProperty("codec_name")
    private String codecName;
    @JsonProperty("codec_long_name")
    private String codecLongName;
    @JsonProperty("profile")
    private String profile;
    @JsonProperty("codec_type")
    private String codecType;
    @JsonProperty("codec_time_base")
    private String codecTimeBase;
    @JsonProperty("codec_tag_string")
    private String codecTagString;
    @JsonProperty("codec_tag")
    private String codecTag;
    @JsonProperty("width")
    private Integer width;
    @JsonProperty("height")
    private Integer height;
    @JsonProperty("coded_width")
    private Integer codedWidth;
    @JsonProperty("coded_height")
    private Integer codedHeight;
    @JsonProperty("has_b_frames")
    private Integer hasBFrames;
    @JsonProperty("sample_aspect_ratio")
    private String sampleAspectRatio;
    @JsonProperty("display_aspect_ratio")
    private String displayAspectRatio;
    @JsonProperty("pix_fmt")
    private String pixFmt;
    @JsonProperty("level")
    private Integer level;
    @JsonProperty("chroma_location")
    private String chromaLocation;
    @JsonProperty("field_order")
    private String fieldOrder;
    @JsonProperty("refs")
    private Integer refs;
    @JsonProperty("is_avc")
    private String isAvc;
    @JsonProperty("nal_length_size")
    private String nalLengthSize;
    @JsonProperty("id")
    private String id;
    @JsonProperty("r_frame_rate")
    private String rFrameRate;
    @JsonProperty("avg_frame_rate")
    private String avgFrameRate;
    @JsonProperty("time_base")
    private String timeBase;
    @JsonProperty("start_pts")
    private Long startPts;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("bits_per_raw_sample")
    private String bitsPerRawSample;
    @JsonProperty("color_range")
    private String colorRange;
    @JsonProperty("disposition")
    private Disposition disposition;
    @JsonProperty("sample_fmt")
    private String sampleFmt;
    @JsonProperty("sample_rate")
    private String sampleRate;
    @JsonProperty("channels")
    private Integer channels;
    @JsonProperty("channel_layout")
    private String channelLayout;
    @JsonProperty("bits_per_sample")
    private Integer bitsPerSample;
    @JsonProperty("bit_rate")
    private String bitRate;
    @JsonProperty("tags")
    private Tags tags;
    @JsonIgnore
    private Map<String, Object> additionalProperties;

    public Streams() {
        additionalProperties = new HashMap<>();
    }

    @JsonProperty("index")
    public Integer getIndex() {
        return index;
    }

    @JsonProperty("index")
    public void setIndex(Integer index) {
        this.index = index;
    }

    @JsonProperty("codec_name")
    public String getCodecName() {
        return codecName;
    }

    @JsonProperty("codec_name")
    public void setCodecName(String codecName) {
        this.codecName = codecName;
    }

    @JsonProperty("codec_long_name")
    public String getCodecLongName() {
        return codecLongName;
    }

    @JsonProperty("codec_long_name")
    public void setCodecLongName(String codecLongName) {
        this.codecLongName = codecLongName;
    }

    @JsonProperty("profile")
    public String getProfile() {
        return profile;
    }

    @JsonProperty("profile")
    public void setProfile(String profile) {
        this.profile = profile;
    }

    @JsonProperty("codec_type")
    public String getCodecType() {
        return codecType;
    }

    @JsonProperty("codec_type")
    public void setCodecType(String codecType) {
        this.codecType = codecType;
    }

    @JsonProperty("codec_time_base")
    public String getCodecTimeBase() {
        return codecTimeBase;
    }

    @JsonProperty("codec_time_base")
    public void setCodecTimeBase(String codecTimeBase) {
        this.codecTimeBase = codecTimeBase;
    }

    @JsonProperty("codec_tag_string")
    public String getCodecTagString() {
        return codecTagString;
    }

    @JsonProperty("codec_tag_string")
    public void setCodecTagString(String codecTagString) {
        this.codecTagString = codecTagString;
    }

    @JsonProperty("codec_tag")
    public String getCodecTag() {
        return codecTag;
    }

    @JsonProperty("codec_tag")
    public void setCodecTag(String codecTag) {
        this.codecTag = codecTag;
    }

    @JsonProperty("width")
    public Integer getWidth() {
        return width;
    }

    @JsonProperty("width")
    public void setWidth(Integer width) {
        this.width = width;
    }

    @JsonProperty("height")
    public Integer getHeight() {
        return height;
    }

    @JsonProperty("height")
    public void setHeight(Integer height) {
        this.height = height;
    }

    @JsonProperty("color_range")
    public String getColorRange() {
        return colorRange;
    }

    @JsonProperty("color_range")
    public void setColorRange(String colorRange) {
        this.colorRange = colorRange;
    }

    @JsonProperty("coded_width")
    public Integer getCodedWidth() {
        return codedWidth;
    }

    @JsonProperty("coded_width")
    public void setCodedWidth(Integer codedWidth) {
        this.codedWidth = codedWidth;
    }

    @JsonProperty("coded_height")
    public Integer getCodedHeight() {
        return codedHeight;
    }

    @JsonProperty("coded_height")
    public void setCodedHeight(Integer codedHeight) {
        this.codedHeight = codedHeight;
    }

    @JsonProperty("has_b_frames")
    public Integer getHasBFrames() {
        return hasBFrames;
    }

    @JsonProperty("has_b_frames")
    public void setHasBFrames(Integer hasBFrames) {
        this.hasBFrames = hasBFrames;
    }

    @JsonProperty("sample_aspect_ratio")
    public String getSampleAspectRatio() {
        return sampleAspectRatio;
    }

    @JsonProperty("sample_aspect_ratio")
    public void setSampleAspectRatio(String sampleAspectRatio) {
        this.sampleAspectRatio = sampleAspectRatio;
    }

    @JsonProperty("display_aspect_ratio")
    public String getDisplayAspectRatio() {
        return displayAspectRatio;
    }

    @JsonProperty("display_aspect_ratio")
    public void setDisplayAspectRatio(String displayAspectRatio) {
        this.displayAspectRatio = displayAspectRatio;
    }

    @JsonProperty("pix_fmt")
    public String getPixFmt() {
        return pixFmt;
    }

    @JsonProperty("pix_fmt")
    public void setPixFmt(String pixFmt) {
        this.pixFmt = pixFmt;
    }

    @JsonProperty("level")
    public Integer getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(Integer level) {
        this.level = level;
    }

    @JsonProperty("chroma_location")
    public String getChromaLocation() {
        return chromaLocation;
    }

    @JsonProperty("chroma_location")
    public void setChromaLocation(String chromaLocation) {
        this.chromaLocation = chromaLocation;
    }

    @JsonProperty("field_order")
    public String getFieldOrder() {
        return fieldOrder;
    }

    @JsonProperty("field_order")
    public void setFieldOrder(String fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    @JsonProperty("refs")
    public Integer getRefs() {
        return refs;
    }

    @JsonProperty("refs")
    public void setRefs(Integer refs) {
        this.refs = refs;
    }

    @JsonProperty("is_avc")
    public String getIsAvc() {
        return isAvc;
    }

    @JsonProperty("is_avc")
    public void setIsAvc(String isAvc) {
        this.isAvc = isAvc;
    }

    @JsonProperty("nal_length_size")
    public String getNalLengthSize() {
        return nalLengthSize;
    }

    @JsonProperty("nal_length_size")
    public void setNalLengthSize(String nalLengthSize) {
        this.nalLengthSize = nalLengthSize;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("r_frame_rate")
    public String getRFrameRate() {
        return rFrameRate;
    }

    @JsonProperty("r_frame_rate")
    public void setRFrameRate(String rFrameRate) {
        this.rFrameRate = rFrameRate;
    }

    @JsonProperty("avg_frame_rate")
    public String getAvgFrameRate() {
        return avgFrameRate;
    }

    @JsonProperty("avg_frame_rate")
    public void setAvgFrameRate(String avgFrameRate) {
        this.avgFrameRate = avgFrameRate;
    }

    @JsonProperty("time_base")
    public String getTimeBase() {
        return timeBase;
    }

    @JsonProperty("time_base")
    public void setTimeBase(String timeBase) {
        this.timeBase = timeBase;
    }

    @JsonProperty("start_pts")
    public Long getStartPts() {
        return startPts;
    }

    @JsonProperty("start_pts")
    public void setStartPts(Long startPts) {
        this.startPts = startPts;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("bits_per_raw_sample")
    public String getBitsPerRawSample() {
        return bitsPerRawSample;
    }

    @JsonProperty("bits_per_raw_sample")
    public void setBitsPerRawSample(String bitsPerRawSample) {
        this.bitsPerRawSample = bitsPerRawSample;
    }

    @JsonProperty("disposition")
    public Disposition getDisposition() {
        return disposition;
    }

    @JsonProperty("disposition")
    public void setDisposition(Disposition disposition) {
        this.disposition = disposition;
    }

    @JsonProperty("sample_fmt")
    public String getSampleFmt() {
        return sampleFmt;
    }

    @JsonProperty("tags")
    public Tags getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(Tags tags) {
        this.tags = tags;
    }

    @JsonProperty("sample_fmt")
    public void setSampleFmt(String sampleFmt) {
        this.sampleFmt = sampleFmt;
    }

    @JsonProperty("sample_rate")
    public String getSampleRate() {
        return sampleRate;
    }

    @JsonProperty("sample_rate")
    public void setSampleRate(String sampleRate) {
        this.sampleRate = sampleRate;
    }

    @JsonProperty("channels")
    public Integer getChannels() {
        return channels;
    }

    @JsonProperty("channels")
    public void setChannels(Integer channels) {
        this.channels = channels;
    }

    @JsonProperty("channel_layout")
    public String getChannelLayout() {
        return channelLayout;
    }

    @JsonProperty("channel_layout")
    public void setChannelLayout(String channelLayout) {
        this.channelLayout = channelLayout;
    }

    @JsonProperty("bits_per_sample")
    public Integer getBitsPerSample() {
        return bitsPerSample;
    }

    @JsonProperty("bits_per_sample")
    public void setBitsPerSample(Integer bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
    }

    @JsonProperty("bit_rate")
    public String getBitRate() {
        return bitRate;
    }

    @JsonProperty("bit_rate")
    public void setBitRate(String bitRate) {
        this.bitRate = bitRate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
