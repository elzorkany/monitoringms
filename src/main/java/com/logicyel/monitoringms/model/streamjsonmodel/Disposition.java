package com.logicyel.monitoringms.model.streamjsonmodel;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "default",
    "dub",
    "original",
    "comment",
    "lyrics",
    "karaoke",
    "forced",
    "hearing_impaired",
    "visual_impaired",
    "clean_effects",
    "attached_pic",
    "timed_thumbnails"
})
@JsonDeserialize
public class Disposition {

    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("dub")
    private Integer dub;
    @JsonProperty("original")
    private Integer original;
    @JsonProperty("comment")
    private Integer comment;
    @JsonProperty("lyrics")
    private Integer lyrics;
    @JsonProperty("karaoke")
    private Integer karaoke;
    @JsonProperty("forced")
    private Integer forced;
    @JsonProperty("hearing_impaired")
    private Integer hearingImpaired;
    @JsonProperty("visual_impaired")
    private Integer visualImpaired;
    @JsonProperty("clean_effects")
    private Integer cleanEffects;
    @JsonProperty("attached_pic")
    private Integer attachedPic;
    @JsonProperty("timed_thumbnails")
    private Integer timedThumbnails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("dub")
    public Integer getDub() {
        return dub;
    }

    @JsonProperty("dub")
    public void setDub(Integer dub) {
        this.dub = dub;
    }

    @JsonProperty("original")
    public Integer getOriginal() {
        return original;
    }

    @JsonProperty("original")
    public void setOriginal(Integer original) {
        this.original = original;
    }

    @JsonProperty("comment")
    public Integer getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(Integer comment) {
        this.comment = comment;
    }

    @JsonProperty("lyrics")
    public Integer getLyrics() {
        return lyrics;
    }

    @JsonProperty("lyrics")
    public void setLyrics(Integer lyrics) {
        this.lyrics = lyrics;
    }

    @JsonProperty("karaoke")
    public Integer getKaraoke() {
        return karaoke;
    }

    @JsonProperty("karaoke")
    public void setKaraoke(Integer karaoke) {
        this.karaoke = karaoke;
    }

    @JsonProperty("forced")
    public Integer getForced() {
        return forced;
    }

    @JsonProperty("forced")
    public void setForced(Integer forced) {
        this.forced = forced;
    }

    @JsonProperty("hearing_impaired")
    public Integer getHearingImpaired() {
        return hearingImpaired;
    }

    @JsonProperty("hearing_impaired")
    public void setHearingImpaired(Integer hearingImpaired) {
        this.hearingImpaired = hearingImpaired;
    }

    @JsonProperty("visual_impaired")
    public Integer getVisualImpaired() {
        return visualImpaired;
    }

    @JsonProperty("visual_impaired")
    public void setVisualImpaired(Integer visualImpaired) {
        this.visualImpaired = visualImpaired;
    }

    @JsonProperty("clean_effects")
    public Integer getCleanEffects() {
        return cleanEffects;
    }

    @JsonProperty("clean_effects")
    public void setCleanEffects(Integer cleanEffects) {
        this.cleanEffects = cleanEffects;
    }

    @JsonProperty("attached_pic")
    public Integer getAttachedPic() {
        return attachedPic;
    }

    @JsonProperty("attached_pic")
    public void setAttachedPic(Integer attachedPic) {
        this.attachedPic = attachedPic;
    }

    @JsonProperty("timed_thumbnails")
    public Integer getTimedThumbnails() {
        return timedThumbnails;
    }

    @JsonProperty("timed_thumbnails")
    public void setTimedThumbnails(Integer timedThumbnails) {
        this.timedThumbnails = timedThumbnails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
