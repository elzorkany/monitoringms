package com.logicyel.monitoringms.model.streamjsonmodel;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"variant_bitrate"
})
public class Tags {

@JsonProperty("variant_bitrate")
private String variantBitrate;
@JsonIgnore
private Map<String, Object> additionalProperties;

    public Tags() {
        additionalProperties = new HashMap<>();
    }

    @JsonProperty("variant_bitrate")
public String getVariantBitrate() {
return variantBitrate;
}

@JsonProperty("variant_bitrate")
public void setVariantBitrate(String variantBitrate) {
this.variantBitrate = variantBitrate;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
