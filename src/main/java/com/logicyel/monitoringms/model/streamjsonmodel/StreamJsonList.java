package com.logicyel.monitoringms.model.streamjsonmodel;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "streams"
})
@JsonDeserialize
public class StreamJsonList {

    @JsonProperty("streams")
    private List<Streams> streams = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties;

    public StreamJsonList() {
        additionalProperties = new HashMap<>();
    }


    @JsonProperty("streams")
    public List<Streams> getStreams() {
        return streams;
    }

    @JsonProperty("streams")
    public void setStreams(List<Streams> streams) {
        this.streams = streams;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

