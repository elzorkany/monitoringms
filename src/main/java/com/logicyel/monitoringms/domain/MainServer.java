package com.logicyel.monitoringms.domain;


import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A mainServer.
 */
@Document(collection = "main_server")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "mainserver")
public class MainServer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("serverType")
    private String serverType;

    @Field("name")
    private String name;

    @Field("token")
    private String token;

    @Field("scan_interval")
    private Long scanInterval;

    @Field("last_scanned")
    private DateTime lastScanned;

    @Indexed(name = "base_url", unique = true)
    @Field("base_url")
    private String baseUrl;

    @Field("email_trigger")
    private String emailTrigger;
    @Field("email_user_ids")
    private List<String> emailUserIds;

    @DBRef
    @Field("subServer")
    private Set<SubServer> subServers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MainServer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public MainServer token(String token) {
        this.token = token;
        return this;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public MainServer baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public Long getScanInterval() {
        return scanInterval;
    }

    public void setScanInterval(Long scanInterval) {
        this.scanInterval = scanInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Set<SubServer> getSubServers() {
        return subServers;
    }

    public MainServer originServers(Set<SubServer> subServers) {
        this.subServers = subServers;
        return this;
    }

    public MainServer addOriginServer(SubServer subServer) {
        this.subServers.add(subServer);
        subServer.setMainServer(this);
        return this;
    }

    public MainServer removeOriginServer(SubServer subServer) {
        this.subServers.remove(subServer);
        subServer.setMainServer(null);
        return this;
    }

    public String getEmailTrigger() {
        return emailTrigger;
    }

    public void setEmailTrigger(String emailTrigger) {
        this.emailTrigger = emailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    public void setSubServers(Set<SubServer> subServers) {
        this.subServers = subServers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MainServer mainServer = (MainServer) o;
        if (mainServer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mainServer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "mainServer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", token='" + getToken() + "'" +
            ", baseUrl='" + getBaseUrl() + "'" +
            ", serverType='" + getServerType() + "'" +
            "}";
    }
}
