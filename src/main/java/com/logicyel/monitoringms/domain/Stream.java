package com.logicyel.monitoringms.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A Stream.
 */
@Document(collection = "stream")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "stream")
public class Stream implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Indexed(name = "base_url", unique = true)
    @Field("url")
    private String url;

    @Field("id_on_server")
    private int idOnServer;
    @Field("inherent_interval")
    private Long inherentInterval;

    @Field("Custom_interval")
    private Long customInterval;

    @Field("last_scanned")
    private DateTime lastScanned;

    @DBRef
    @Field("subServer")
    @JsonIgnoreProperties("streams")
    @JsonProperty
    private SubServer subServer;

    @Field("status")
    private String status;

    @Field("inherent_email_trigger")
    private String inherentEmailTrigger;

    @Field("custom_email_trigger")
    private String customEmailTrigger;

    @Field("email_user_ids")
    private List<String> emailUserIds;

    @DBRef
    @Field("ScanArchive")
    @JsonIgnore
    private Set<ScanArchive> scanArchives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Stream name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public Stream url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdOnServer() {
        return idOnServer;
    }

    public Stream idOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
        return this;
    }

    public void setIdOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
    }

    public SubServer getSubServer() {
        return subServer;
    }

    public Stream subServer(SubServer subServer) {
        this.subServer = subServer;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInherentEmailTrigger() {
        return inherentEmailTrigger;
    }

    public void setInherentEmailTrigger(String inherentEmailTrigger) {
        this.inherentEmailTrigger = inherentEmailTrigger;
    }

    public String getCustomEmailTrigger() {
        return customEmailTrigger;
    }

    public void setCustomEmailTrigger(String customEmailTrigger) {
        this.customEmailTrigger = customEmailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    public Long getInherentInterval() {
        return inherentInterval;
    }

    public void setInherentInterval(Long inherentInterval) {
        this.inherentInterval = inherentInterval;
    }

    public Long getCustomInterval() {
        return customInterval;
    }

    public void setCustomInterval(Long customInterval) {
        this.customInterval = customInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    public void setSubServer(SubServer subServer) {
        this.subServer = subServer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Set<ScanArchive> getScanArchives() {
        return scanArchives;
    }

    public void setScanArchives(Set<ScanArchive> scanArchives) {
        this.scanArchives = scanArchives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stream stream = (Stream) o;

        if (stream.getName() == null || getName() == null || stream.getUrl() == null || getUrl() == null) {
            return false;
        }
        return name.equals(stream.name) &&
            url.equals(stream.url);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, url, idOnServer);
    }

    @Override
    public String toString() {
        return "Stream{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", idOnServer='" + getIdOnServer() + "'" +
            "}";
    }
}
