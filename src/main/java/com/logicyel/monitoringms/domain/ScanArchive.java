package com.logicyel.monitoringms.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ScanArchive.
 */
@Document(collection = "scan_archive")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "scanarchive")
public class ScanArchive implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("scan_time")
    private DateTime scanTime;

    @Field("previous_state")
    private String previousState;

    @Field("current_state")
    private String currentState;

    @DBRef
    @Field("stream")
    @JsonIgnoreProperties("ScanArchives")
    @JsonProperty
    private Stream stream;



    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getScanTime() {
        return scanTime;
    }

    public void setScanTime(DateTime scanTime) {
        this.scanTime = scanTime;
    }

    public ScanArchive scanTime(DateTime scanTime) {
        this.scanTime = scanTime;
        return this;
    }


    public String getPreviousState() {
        return previousState;
    }

    public ScanArchive previousState(String previousState) {
        this.previousState = previousState;
        return this;
    }

    public void setPreviousState(String previousState) {
        this.previousState = previousState;
    }

    public String getCurrentState() {
        return currentState;
    }

    public ScanArchive currentState(String currentState) {
        this.currentState = currentState;
        return this;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public Stream getStream() {
        return stream;
    }

    public ScanArchive streamLogs(Stream stream) {
        this.stream = stream;
        return this;
    }

    public void setStream(Stream stream) {
        this.stream = stream;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ScanArchive scanArchive = (ScanArchive) o;
        if (scanArchive.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scanArchive.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ScanArchive{" +
            "id=" + getId() +
            ", scanTime='" + getScanTime() + "'" +
            ", previousState='" + getPreviousState() + "'" +
            ", currentState='" + getCurrentState() + "'" +
            "}";
    }
}
