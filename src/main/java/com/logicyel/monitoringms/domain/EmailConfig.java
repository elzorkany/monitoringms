package com.logicyel.monitoringms.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EmailConfig.
 */
@Document(collection = "email_config")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "emailconfig")
public class EmailConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("host")
    private String host;

    @Field("port")
    private Integer port;

    @Field("protocol")
    private String protocol;

    @Field("username")
    private String username;

    @Field("password")
    private String password;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public EmailConfig host(String host) {
        this.host = host;
        return this;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public EmailConfig port(Integer port) {
        this.port = port;
        return this;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public EmailConfig protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUsername() {
        return username;
    }

    public EmailConfig username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public EmailConfig password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmailConfig emailConfig = (EmailConfig) o;
        if (emailConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emailConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmailConfig{" +
            "id=" + getId() +
            ", host='" + getHost() + "'" +
            ", port=" + getPort() +
            ", protocol='" + getProtocol() + "'" +
            ", username='" + getUsername() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }
}
