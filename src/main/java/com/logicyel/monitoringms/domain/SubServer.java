package com.logicyel.monitoringms.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A SubServer.
 */
@Document(collection = "sub_server")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "subserver")
public class SubServer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Indexed(name = "base_url", unique = true)
    @Field("base_url")
    private String baseUrl;

    @Field("id_on_server")
    private int idOnServer;

    @Field("inherent_interval")
    private Long inherentInterval;

    @Field("Custom_interval")
    private Long customInterval;

    @Field("last_scanned")
    private DateTime lastScanned;

    @Field("inherent_email_trigger")
    private String inherentEmailTrigger;

    @Field("custom_email_trigger")
    private String customEmailTrigger;

    @Field("email_user_ids")
    private List<String> emailUserIds;
    @DBRef
    @Field("mainServer")
    @JsonIgnoreProperties("subServers")
    @JsonProperty
    private MainServer mainServer;

    @DBRef
    @Field("stream")
    @JsonIgnore
    private Set<Stream> streams = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SubServer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public SubServer baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }


    public Long getInherentInterval() {
        return inherentInterval;
    }

    public void setInherentInterval(Long inherentInterval) {
        this.inherentInterval = inherentInterval;
    }

    public Long getCustomInterval() {
        return customInterval;
    }

    public void setCustomInterval(Long customInterval) {
        this.customInterval = customInterval;
    }

    public DateTime getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(DateTime lastScanned) {
        this.lastScanned = lastScanned;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public MainServer getMainServer() {
        return mainServer;
    }

    public SubServer idOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
        return this;
    }


    public void setMainServer(MainServer mainServer) {
        this.mainServer = mainServer;
    }

    public Set<Stream> getStreams() {
        return streams;
    }

    public SubServer streams(Set<Stream> streams) {
        this.streams = streams;
        return this;
    }

    public SubServer addStream(Stream stream) {
        this.streams.add(stream);
        stream.setSubServer(this);
        return this;
    }

    public SubServer removeStream(Stream stream) {
        this.streams.remove(stream);
        stream.setSubServer(null);
        return this;
    }

    public String getInherentEmailTrigger() {
        return inherentEmailTrigger;
    }

    public void setInherentEmailTrigger(String inherentEmailTrigger) {
        this.inherentEmailTrigger = inherentEmailTrigger;
    }

    public String getCustomEmailTrigger() {
        return customEmailTrigger;
    }

    public void setCustomEmailTrigger(String customEmailTrigger) {
        this.customEmailTrigger = customEmailTrigger;
    }

    public List<String> getEmailUserIds() {
        return emailUserIds;
    }

    public void setEmailUserIds(List<String> emailUserIds) {
        this.emailUserIds = emailUserIds;
    }

    public void setStreams(Set<Stream> streams) {
        this.streams = streams;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public int getIdOnServer() {
        return idOnServer;
    }

    public void setIdOnServer(int idOnServer) {
        this.idOnServer = idOnServer;
    }

    public SubServer mainServer(MainServer mainServer) {
        this.mainServer = mainServer;
        return this;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubServer that = (SubServer) o;
        if (that.getName() == null || getName() == null ||  that.getBaseUrl() == null || getBaseUrl() == null) {
            return false;
        }
        return name.equals(that.name) &&
            baseUrl.equals(that.baseUrl) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getBaseUrl(), getIdOnServer());
    }

    @Override
    public String toString() {
        return "SubServer{" +
            "id=" + getId() +
            ", idOnServer=" + getIdOnServer() +
            ", name='" + getName() + "'" +
            ", baseUrl='" + getBaseUrl() + "'" +
            ", mainServer='" + getMainServer() + "'" +
            "}";
    }
}
