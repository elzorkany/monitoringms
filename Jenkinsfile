#!/usr/bin/env groovy

node('DevServer') {
    stage('checkout') {
        checkout scm
    }

    stage('check java') {
        sh "java -version"
    }

    stage('clean') {
        sh "chmod +x mvnw"
        sh "./mvnw clean"
    }

    stage('backend tests') {
        try {
            sh "./mvnw test"
        } catch(err) {
            throw err
        } finally {
            junit '**/target/surefire-reports/TEST-*.xml'
        }
    }

    stage('packaging') {
        sh "./mvnw verify -Pprod -DskipTests"
        archiveArtifacts artifacts: '**/target/*.war', fingerprint: true
    }
    stage('quality analysis') {
        withSonarQubeEnv('sonarqube') {
            sh "./mvnw sonar:sonar"
        }
    }

    def dockerImage
    stage('build docker') {
        sh "cp -R src/main/docker target/"
        sh "cp target/*.war target/docker/"
        dockerImage = docker.build('logicyel/monitoringms', 'target/docker')
    }

    stage('publish docker') {
        docker.withRegistry('https://hub.theeclipse.xyz', 'docker-login') {
            dockerImage.push 'latest'
        }
    }

    stage('deploy') {
        rancher confirm: true,
            credentialId: 'rancher-login',
            endpoint: 'https://manage.theeclipse.xyz:8443/v2-beta',
            environmentId: '1a5',
            environments: '',
            image: 'hub.theeclipse.xyz/logicyel/monitoringms',
            ports: '',
            service: 'monitoring-dev/monitoringms-app',
            timeout: 200
    }
}
